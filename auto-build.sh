#!/bin/bash

##
## Automatic build process
## 
## This script is used to update the build everytime the source is changed.
## 

FILE_TIME="./build/autobuild.time"
FILE_LIST="./build/autobuild.list"

echo -e "\e[0;33m>>> running automatic build process (use ctrl+c to exit)\e[0m"

# if there is no autobuild timestamp file yet, create it
if [ ! -f "${FILE_TIME}" ];
then
	echo -e "info: created initial timestamp file ${FILE_TIME}"
	touch "${FILE_TIME}"
fi

TIMESTAMP=`stat -c "%x" "${FILE_TIME}"`;
echo -e "looking for changes made after: ${TIMESTAMP}"

# endless loop
while :
do

TIMESTAMP=`stat -c "%x" "${FILE_TIME}"`;
find ./src -newermt "${TIMESTAMP}" > "${FILE_LIST}"
CHANGES=`cat "${FILE_LIST}" | wc -l`

if [ "${CHANGES}" -gt 0 ]; then
	NOW=`date +%H:%M:%S`
	echo -e "\e[0;33m>>> ${NOW} - ${CHANGES} change(s) found:\e[0m"
	cat "${FILE_LIST}"
	make update-build > /dev/null
	touch "${FILE_TIME}"
fi

sleep 1

done
