#!/bin/bash

##
## Minify
## 
## This script is used to minify js or css files.
## 

# exclude startup files (or javascript will be disabled completely)
if [[ "$1" == *"httpdocs/resources/src/startup/"* ]]; then
        echo "$1 -- skipped"
        exit
fi

echo $1

yui-compressor --line-break 120 $1 -o $1 > /dev/null 2>&1