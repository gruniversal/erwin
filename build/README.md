# build

This directory is used for temporary file operations during the build process.

See the [Makefile](../Makefile) for more information.
