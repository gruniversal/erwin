# Changelog

The changelog contains all major changes in the project.

All released versions are listed here.

## Release v0.4.9

This version consists of security fixes and minor changes.

### 2023-10-10

**MediaWiki**
* updated MediaWiki core to 1.35.12

**Matomo Opt Out**
* moved to separate git: https://gitlab.wikimedia.org/gruniversal/mediawiki-extensions-matomooptout

## Release v0.4.8

This version consists of security fixes and minor changes.

### 2022-11-22

**MediaWiki**
* updated MediaWiki core to 1.35.8

### 2022-05-19

**MatomoOptOut v4.4.2**
* fix script path for `MatomoOptOut.js` (now also works if `$wgScriptPath` is set)
* fix js file URL for old naming `piwik.php`

## Release v0.4.7

This version consists of security fixes and minor changes.

### 2022-04-19

**MediaWiki**
* updated MediaWiki core to 1.35.6

## Release v0.4.6

This version fixed the Matomo opt-out script.

### 2022-03-15

**MatomoOptOut v4.4.0**
* fixed url for `MatomoOptOut.js` and rechanged extension name from MatomoOptout to MatomoOptOut for correct CamelCase naming
* added a check to wait for `_paq` to be initialized
* slightly improved console log messages

**Development**
* removed pinning for extension [DynamicPageList](https://www.mediawiki.org/wiki/Extension:DynamicPageList3) (since not necessary any more)

## Release v0.4.5

This release renamed the extension Matomo to [MatomoOutput](src/extensions/MatomoOutput) and added a security fix for DPL3.

### 2022-02-04

**Security**
* updated extension [DynamicPageList](https://www.mediawiki.org/wiki/Extension:DynamicPageList3) to version 3.3.8 to fix possible ReDoS weakness (advisory: https://github.com/Universal-Omega/DynamicPageList3/security/advisories/GHSA-8f24-q75c-jhf4)

**Development**
* harmonized version configuration in [Makefile](Makefile)
* updated shrink process

### 2022-01-13

**Matomo Optout v4.3.0**
* changed extension name from Matomo to MatomoOptout to make it distinguishable from the original extension
* dropped composer support (for now)

**Development**
* changed vendor source for extension [DynamicPageList](https://www.mediawiki.org/wiki/Extension:DynamicPageList3)

## Release v0.4.4

This version consists of security fixes and minor changes.

### 2021-12-18

**MediaWiki**
* updated MediaWiki core to 1.35.5

## Release v0.4.3

This version focuses on security and minor updates.

### 2021-10-04

**MediaWiki**
* updated MediaWiki core to 1.35.4

### 2021-08-22

**Dryad Skin**
* replaced favicon with newer version (blue instead of green)

### 2021-07-12

**Development**
* re-enabled jquery migrate since WikiEditor does not work if disabled (see also: https://phabricator.wikimedia.org/T213426)

## Release v0.4.2

This version focuses on security and maintenance updates.

### 2021-07-08

**Development**
* delete unneeded JavaScript locales as well (preserve de and en)
* disable NewPP limit report (see: `$wgEnableParserLimitReporting`)
* added experimental css/js minify task to [Makefile](Makefile)
* removed performance measurement scripts

### 2021-07-06

**Mediawiki**
* updated MediaWiki core to 1.35.3

## Release v0.4.1

This version brings minor design updates.

### 2021-05-29

**Dryad Skin**
* images are not longer stretched if wider than container size
* set thumbnail image background to white
* remove unwanted border from global container

## Release v0.4

This release features the new Employees for Future corporate design. It also brings a new extension to add the E4F-Roadmap box as parser function.

### 2021-05-17

**ErWiN Roadmap**
* changed layout and added new CI colors
* changed fixed texts (shorted a bit)
* updated documentation for changed color names

**Development**
* replaced deprecated method `user->isLoggedIn()` with `user->Registered()`

### 2021-05-11

**Dryad Skin**
* even more CI stuff

### 2021-05-07

**Dryad Skin**
* more CI stuff

### 2021-05-06

**Dryad Skin**
* added new ErWiN logo
* added new fonts
* applied new CI to screen

### 2021-05-05

**ErWiN Roadmap**
* added final roadmap colors

### 2021-04-18

**ErWiN Roadmap**
* added first version of extension [ErwinRoadmap](src/extensions/ErwinRoadmap) to display roadmap info block via parser function

## Release v0.3.3

This release focuses on security and bug fixes and raised core to Mediawiki 1.35.2.

### 2021-04-14

**Dryad Skin**
* removed use of deprecated global `$wgUser`

### 2021-04-13

**MediaWiki**
* updated MediaWiki core to 1.35.2
* updated patches, languages overlay and db schema for MediaWiki 1.35.2

**Matomo**
* updated extension [Matomo](src/extensions/Matomo) to version 4.2.4 (removed use of deprecated global `$wgUser`)

**Development**
* phpcs now also displays warnings but some are ignored (especially those related to spacing issues)
* fixed more coding styles and simplified makefile use (`make check-code`)

### 2021-04-02

**Dryad Skin**
* fixed stretched images in tables

**Development**
* added support phpcs ([PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)) to the project to check own skins and extensions against the MediaWiki Standard
  * this requires composer and phpcs to be installed on your system
  * read [more about coding conventions for MediaWiki](https://www.mediawiki.org/wiki/Manual:Coding_conventions/PHP)
  * for now it only displays errors, warnings are ignored
* fixed some coding styles, skipped some for better readability

## Release v0.3.2

This version adds the new [SharePage](src/extensions/SharePage) extension to display social media sharing buttons at the bottom of each article.

### 2021-02-24

**SharePage**
* changed handling of JavaScript actions
* improved documentation and readability

### 2021-02-17

**SharePage**
* added more services and removed not working ones
* limit display to pages in `$wgSharePageAllowedNamespaces`
* set default configuration in `extension.json`
* initial documentation

### 2021-02-07

**SharePage**
* created new extension [SharePage](src/extensions/SharePage) for simple social sharing buttons (prototype)

**Dryad Skin**
* fixed background color above footer

## Release v0.3.1

ErWiN now has the new [Employees for Future](https://www.employeesforfuture.org) logo and favicon. Also some improvements in [Matomo](src/extensions/Matomo) extension.

### 2021-02-07

**Matomo**
* show message if [Matomo](src/extensions/Matomo) is disabled

**Vendor extensions**
* [ApprovedRevs](https://www.mediawiki.org/wiki/Extension:ApprovedRevs): Remove support for MW < 1.31, improve support for MW 1.35

### 2021-02-06

**Matomo**
* fixed falsely embedding opt out script if Matomo is disabled
* some more refactoring: visibility and order of methods, naming, typo

### 2021-02-04

**MediaWiki**
* set option to ignore users with login in Matomo tracking

**Dryad Skin**
* changed logo files to new version (now also supports HiDPI logo)
* added favicon and apple touch icon

**Matomo**
* add additional languages to opt-out script

**Development**
* load all extensions with `wfLoadExtension()`

## Release v0.3

Integrated and enabled Matomo analytics for fully anonymized and gdpr compliant user tracking.

### 2021-01-31

**Development**
* added Matomo settings to `LocalSettings.php` again since loading mechanism isn't brilliant right now

### 2021-01-28

**Development**
* improved documentation for [Matomo](src/extensions/Matomo) extension and successful integration
* added sql statements for Matomo: basic schema and root user access (if the setup wizard does not work for some reason)

### 2021-01-24

**Matomo**
* cloned extension [Matomo](https://www.mediawiki.org/wiki/Extension:Matomo) to `src/extensions/Matomo` for additional development
* added parser tag `<matomo-optout />` to Matomo extension
* simplified path handling and allowed for full URLs (backwards compatible)
* restructured Matomo callbacks and custom js
* removed unused parameters, more cleanup and code readability

### 2021-01-23

**MediaWiki**
* added hook to add `google-site-verification` meta tag to `LocalSettings.php` (is used to verify ownership in Google Search Console)
* added extension [Matomo](https://www.mediawiki.org/wiki/Extension:Matomo) to enable Matomo tracking

**Dryad Skin**
* fixed layout issue in mobile devices with narrow text columns for portrait mode images in articles

**Development**
* updated `sql/db_schema.sql` for MediaWiki 1.35
* added `update-matomo` and `symlink-matomo` to retrieve and integrate Matomo core
* added sql statements for a GDPR compliant Matomo setup (see: `sql/matomo_gdpr.sql`)

### 2021-01-20

**Development**
* fixed language overlay for MediaWiki 1.35

## Release v0.2

This version updates ErWiN to MediaWiki 1.35.1 for LTS support.

### 2020-12-29

**Development**
* added `retrieve-database` option to [Makefile](Makefile) to retrieve database dumps from LIVE for backup (uses [ftp-mysql-dump](https://gitlab.com/gruniversal/ftp-mysql-dump))

### 2020-12-28

**MediaWiki**
* updated MediaWiki core to 1.35.1
* updated extensions to work with MediaWiki 1.35
  * **breaking change** in [Description2](https://www.mediawiki.org/wiki/Extension:Description2): `<metadesc>` is no longer supported, use `{{#description2:xxx}}` instead
* repatched [ApprovedRevs](https://www.mediawiki.org/wiki/Extension:ApprovedRevs) extension to re-enable `$egApprovedRevsBlankPage`

**Dryad Skin**
* updated skin [Timeless](https://www.mediawiki.org/wiki/Skin:Timeless) to work with MediaWiki 1.35

**Development**
* added new build options to [Makefile](Makefile) for builds with maintenance tools
* added `$wgUpgradeKey` to `LocalSettings.php` to allow usage of web installer (for database updates)
* updated self check script for higher requirements (PHP >= 7.3.19) and more complete list of mandatory php extensions
* slightly improved build shrinking in [Makefile](Makefile)
* updated documentation

## Release v0.1.1

This version improves support for open graph tags and enables the internal mail feature for admins and bureaucrats.

### 2020-12-06

**MediaWiki**
* allowed admins and bureaucrats to send emails via "Spezial:E-Mail_senden"
* external links are now always opened in a new tab
* added extension [Description2](https://www.mediawiki.org/wiki/Extension:Description2) to support og:description and make it customizable via `<metadesc>...</metadesc>`

## Release v0.1

This version is the first official version.

### 2020-09-29

**MediaWiki**
* disabled htaccess password for go live :)

### 2020-09-28

**MediaWiki**
* removed link to all categories from main navigation
* added links to guidelines and terms of use to main navigation

## Release v0.0.6

This version adds extensions for generation of dynamic lists and reference lists. It also improves the display of unapproved pages and fixes a core bug that prevented file searches with uppercase chars or umlauts. Furthermore the core is updated to 1.34.4 for improved security.

### 2020-09-26

**MediaWiki**
* added option to configure contents being displayed by [ApprovedRevs](https://www.mediawiki.org/wiki/Extension:ApprovedRevs) via `$egApprovedRevsBlankPage` instead of a blank page
* updated to MediaWiki 1.34.4 (security release, fixes some security issues)
* set `$wgForceHTTPS = true;` to ensure redirect of http requests (new feature in MediaWiki 1.34.4)

### 2020-09-17

**MediaWiki**
* changed URL for LIVE to https://erwin.employeesforfuture.org
* enforced use of https in `htaccess`

### 2020-08-17

**MediaWiki**
* added extension [Cite](https://www.mediawiki.org/wiki/Extension:Cite) for references / link lists
* fixed searching for upper case chars in Special:FileList and Special:NewFiles (core patch)

### 2020-07-12

**MediaWiki**
* added extension [DynamicPageList](https://www.mediawiki.org/wiki/Extension:DynamicPageList3) for generation of dynamic lists (e.g. list of pages in a category)

## Release v0.0.5

This version was used for the limited go live. It updates MediaWiki to the newest security release and fixes some minor bugs. It also adds the [CardLayout](src/extension/CardLayout) extension for a basic responsive card layout.

### 2020-07-10

**CardLayout**
* bugfix: added newline to code to make wiki markup work (e.g. bullet lists)

### 2020-07-08

**CardLayout**
* renamed extension Cards to [CardLayout](src/extension/CardLayout) (name was already taken)
* added [README.md](src/extension/CardLayout/README.md) for documentation

**Development**
* added version information and improved code readability for CardLayout and Dryad Skin

### 2020-07-06

**MediaWiki**
* added extension ParserFunctions to use with CardLayout

**Cards**
* developed first version of Cards Extension
* moved basic card layout styles from Dryad Skin to Cards extension
* added support for simpler tag: {{#cards: }}
* tag {{#card: }} may also have only one parameter

### 2020-07-05

**MediaWiki**
* added hook to set "remember me" checkbox as default in login form

**Dryad Skin**
* sorted menu entries
* added links to categories and about for all users
* added link to FAQs for logged in users
* added link to user list for bureaucrats
* removed "random page" from sidebar navigation

**Development**
* refactored menu generation

### 2020-06-26

**MediaWiki**
* updated to MediaWiki 1.34.2 (security release, fixes CVE-2020-12051)
* added some password policies

**Dryad Skin**
* replaced sandbox link ("Special:Sandbox" --> "ErWiN:Sandbox")

**Development**
* added setup for LIVE environment
* changed cookie lifetime to 90 days
* set caching to CACHE_DB for all types (disable apc for now)
* set a cookie prefix to prevent using the database name
* added session, cache and cookie information to self check
* fixed self check for non-https environment
* disabled performance measurement by default

### 2020-06-17

**Development**
* git: ignore files for LIVE environment

## Release v0.0.4

For the upcoming testing period this release focused on minor improvements and bug fixes. It also introduced [Open Graph Tags](https://www.mediawiki.org/wiki/Extension:OpenGraphMeta) for better integration of social media services like Facebook or Twitter.

### 2020-06-16

**MediaWiki**
* added a sandbox page for testing period in left navigation
* disabled automatic approvals for users with "approverevisions" privilege
* changed "ErWiN:Lizenzhinweise" to "ErWiN:Urheberrechte" (because this naming was already used in edit form)

**Dryad Skin**
* minor styling changes for [ApprovedRevs](https://www.mediawiki.org/wiki/Extension:ApprovedRevs)

### 2020-06-10

**MediaWiki**
* hidden pages from anonymous users that have no approved revision

**Dryad Skin**
* added some css to improve display of links in [ApprovedRevs](https://www.mediawiki.org/wiki/Extension:ApprovedRevs)
* improved styling of recent changes for mobile devices

### 2020-06-08

**MediaWiki**
* allowed quality guards to move, delete or undelete pages
* removed captcha for adding external URLs
* added extension [OpenGraphMeta](https://www.mediawiki.org/wiki/Extension:OpenGraphMeta) for support of og-tags (used for social media services like facebook or twitter)
* added extension [PageImages](https://www.mediawiki.org/wiki/Extension:PageImages) to support image information (used in OpenGraphMeta)

**Development**
* fixed preview mode (got broken with performance measurement)
* deleted some more build related files in `shrink-build`

## Release v0.0.3

ErWiN now uses an [approval process](README.md#approved-page-versions) for quality assurance of contents. A new user group "quality guards" was introduced therefore.

### 2020-06-06

**MediaWiki**
* added extension [ApprovedRevs](https://www.mediawiki.org/wiki/Extension:ApprovedRevs) to mark page versions as approved
* approvals are done by a new group of users "quality guards"
* added quality assurance link to left nav for quality guards
* restricted pages in namespace "ErWiN:" to `approverevisions` privilege
* changed "Lizenzhinweise" to "ErWiN:Lizenzhinweise"

**Development**
* added tables for extension [ApprovedRevs](https://www.mediawiki.org/wiki/Extension:ApprovedRevs) to database schema
* altered `db_schema.sql` to be executed more than once (`CREATE TABLE IF NOT EXISTS`)
* manual database changes are no longer necessary

### 2020-06-05

**MediaWiki**
* restrict available languages to de (for now)
* added page "Lizenzhinweise" and renamed "Haftungsausschluss" to "Impressum"

**Development**
* added performance measurement for testing purposes (in `.htaccess`)
* use language overlay for group captions (instead of database)
* separated [CHANGELOG.md](CHANGELOG.md) from [README.md](README.md)

## Release v0.0.2

This version adds the new registration process. Users cannot register themselves anymore but can request an user account. Account requests are confirmed by bureaucrats (yes, it's really this wording in MediaWiki).

### 2020-06-04

**MediaWiki**
* disabled default mail authentication
* added registration and login links to left navigation for anonymous users
* added user confirmation and rights management to left nav for bureaucrats
* improved user information on site and within mails
* improved ConfirmAccount settings
* disallowed bureaucrats to disable admins
* disallowed newbies to move pages

**Dryad Skin**
* some styling for user registration process

**Development**
* fixed self check script

### 2020-06-03

**MediaWiki**
* first integration of [ConfirmAccount](https://www.mediawiki.org/wiki/Extension:ConfirmAccount) extension
* use own i18n files for ConfirmAccount extension

**Dryad Skin**
* highlight new user messages

**Development**
* fixed shrinking i18n files in [Makefile](Makefile)

## Release v0.0.1

First version released on GitLab. This version was used for internal previews to determine if the project is on track and what changes are needed before testing period.

### 2020-05-27

**Development**
* reset git history

### 2020-05-26

**Development**
* deleted unnecessary language files from `httpdocs` (reduced build size by 10 MB)
* Makefile: copy `vendor/mediawiki` completely and shrink it later

### 2020-05-25

**MediaWiki**
* added `.htaccess` for short URLs in root directory setup

**Development**
* deleted unnecessary extensions from `httpdocs` (reduced build size by 28 MB)
* deleted unnecessary languages from `httpdocs` (reduced build size by 64 MB)
* deleted other unnecessary files from `httpdocs` (reduced build size by 4 MB)
* changed build process to copy own sources instead of symlinking them
* added autobuild script to continuously update the build while developing
* first version of deployment process

### 2020-05-24

**Development**
* deleted tests from `httpdocs` (reduced build size by 16 MB)

### 2020-05-19

**Development**
* transfered ErWiN repo to GitLab
* supplemented documentation and converted uniformly to english

### 2020-05-18

**Development**
* self check added to check basic system parameters
* database schema and admin user stored as SQL dumps
* bugfix: APC support breaks the admin login, therefore only partially activated

### 2020-05-16

**MediaWiki**
* added captcha query for incorrect logins, creation of user accounts and external URLs

**Dryad Skin**
* background image made invisible (replaced by grey heart)
* small adjustments for captcha display

**Development**
* default ErWiN settings ([`ErWiNSettings.php`](src/erwin/ErWiNSettings.php)) separated from site-specific settings
* added httpdocs to `.gitignore` (only README.md is kept)
* automatic detection of PHP's APC extension for improved caching
* directory structure changed: folder `erwin` inserted, `setup` deleted

### 2020-05-15

**MediaWiki**
* limited various user rights by whitelisting
* specific user group `file-manager` created for file uploads
* deactivated write access to the API for all users
* extension [MsUpload](https://www.mediawiki.org/wiki/Extension:MsUpload) added for easier file uploads

**Dryad Skin**
* included additional links for file managers in the menu

**Development**
* [Makefile](Makefile) created for local build process
* in this context, the directory structure has been completely revised

### 2020-05-14

**MediaWiki**
* templating evaluated: https://www.mediawiki.org/wiki/Help:Templates/de

**Dryad Skin**
* bugfix: suppressed notices with empty menus (for anonymous visitors)

### 2020-05-12

**MediaWiki**
* hidden unnecessary user settings in the backend
* extension [WikiEditor](https://www.mediawiki.org/wiki/Extension:WikiEditor) added for better editing
* extension [TinyMCE](https://www.mediawiki.org/wiki/Extension:TinyMCE) (WYSIWYG) included and evaluated, but partially destroyed the markup, therefore removed again

**Dryad Skin**
* extended functions (e.g. special pages, source text) hidden for anonymous visitors
* form styles improved further
* bugfix: images no longer expand across the page width

### 2020-05-11

**ErWiN's birthday**
* initial implementation based on MediaWiki 1.34
* including first version of the Dryad Skin
