# Dryad Skin

Dryad Skin is a [MediaWiki](https://www.mediawiki.org/) skin, based on [Timeless Skin](https://www.mediawiki.org/wiki/Skin:Timeless) from Isarra Yos.

It is used as a green version of Timeless for the non profit organization [Employees for Future](https://www.employeesforfuture.org).

Author:
* David Gruner, https://www.gruniversal.de

License:
* https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

## Remarks

[Dryad Skin](src/skins/Dryad/README.md) overrides properties (like CSS styles or menu generation) of the Timeless Skin without making any changes to Timeless itself.

The development is mainly based on these ideas:

* http://blog.redwerks.org/2012/02/28/mediawiki-subskin-tutorial/
* https://www.mediawiki.org/wiki/Manual:Skinning_Part_2

Further information can be found in the code.
