<?php

/**
 * SharePage
 *
 * This extension adds social media buttons at the end of each article.
 *
 * @see: https://gitlab.com/gruniversal/erwin/-/tree/master/src/extensions/SharePage/
 *
 * @license	https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author	David Gruner, https://gruniversal.de
 *
 */
class SharePage {

	/** @var array	available services and their configuration */
	private static $knownServices = [
		'copylink' => [
			'url' => 'javascript:navigator.clipboard.writeText("%raw-url%");',
			'bg-color' => '#999999'
		],
		'email' => [
			'url' => 'mailto:?subject=%raw-comment%&body=%raw-url%',
			'bg-color' => '#999999'
		],
		'facebook' => [
			'url' => 'https://www.facebook.com/sharer/sharer.php?u=%url%',
			'bg-color' => '#3C589A'
		],
		'facebookmessenger' => [
			'url' => 'https://www.facebook.com/dialog/send?app_id=1904103319867886&display=popup&link=%url%&redirect_uri=%url%',
			'bg-color' => '#3C589A'
		],
		'linkedin' => [
			'url' => 'https://www.linkedin.com/sharing/share-offsite/?url=%url%',
			'bg-color' => '#0077B5'
		],
		'reddit' => [
			'url' => 'https://www.reddit.com/submit?url=%url%&title=%comment%',
			'bg-color' => '#F44103'
		],
		'telegram' => [
			'url' => 'https://telegram.me/share/url?url=%url%&text=%comment%',
			'bg-color' => '#34AAE6'
		],
		'threema' => [
			'url' => 'threema://compose?text=%comment%%url%',
			'bg-color' => '#3B3B3B'
		],
		'twitter' => [
			'url' => 'https://twitter.com/intent/tweet?text=%comment%%url%',
			'bg-color' => '#55ACEE'
		],
		'xing' => [
			'url' => 'https://www.xing.com/spi/shares/new?url=%url%',
			'bg-color' => '#197172'
		],
		'whatsapp' => [
			'url' => 'https://web.whatsapp.com/send?text=%comment%%url%',
			'bg-color' => '#55EB4C'
		],
	];

	/** @var array	collect services to display */
	private static $myServices = [];

	/** @var bool	sharing is only shown in $wgSharePageAllowedNamespaces */
	private static $isAllowedNamespace = false;

	/**
	 * Hook: Check if namespace matches allowed namespaces
	 *
	 * @param      WikiPage  $article
	 * @param      mixed     $row
	 *
	 * @return     bool      true
	 */
	public static function onArticlePageDataAfter( WikiPage $article, $row ) {

		global $wgSharePageAllowedNamespaces;

		// convert strings like NS_MAIN to their integer value
		foreach ( $wgSharePageAllowedNamespaces as $key => $item ) {
			if ( is_string( $item ) ) {
				$wgSharePageAllowedNamespaces[$key] = constant( $item );
			}
		}

		// check if namespace is allowed
		$namespace = $article->getTitle()->getNamespace();
		if ( in_array( $namespace, $wgSharePageAllowedNamespaces, true ) ) {
			self::$isAllowedNamespace = true;
		}

		return true;
	}

	/**
	 * Hook: Insert sharing icons
	 *
	 * @param      string  &$html
	 * @param      Skin    $skin
	 *
	 * @return     bool    true
	 */
	public static function onSkinAfterContent( string &$html, Skin $skin ) {

		// skip if sharing buttons are not displayed in this namespace
		if ( false === self::$isAllowedNamespace ) {
			return true;
		}

		global $wgSharePageServices, $wgSharePageTitle, $wgSharePageTextLeft;

		// start
		$html .= "<div class='sp-container'>";
		if ( trim( $wgSharePageTitle ) ) {
			$html .= "<div class='sp-title'>${wgSharePageTitle}</div>";
		}
		$html .= "<ul class='sp-buttonlist'>";

		// add text left of buttons
		if ( trim( $wgSharePageTextLeft ) ) {
			$html .= "<li class='sp-button'>";
			$html .= "<i class='sp-button-text'>";
			$html .= "${wgSharePageTextLeft}";
			$html .= '</i></li>';
			$html .= '</li>';
		}

		// set up list of services
		self::buildServiceList( $wgSharePageServices );

		// add buttons
		foreach ( self::$myServices as $name => $service ) {
			$html .= self::addShareButton( $name, $service );
		}

		// end
		$html .= "</ul>";
		$html .= "<div style='clear: both;'></div>";
		$html .= "</div>";

		return true;
	}

	/**
	 * Hook: Insert stylesheet
	 *
	 * @param      OutputPage  $out
	 * @param      Skin        $skin
	 *
	 * @return     bool        true
	 */
	public static function onBeforePageDisplay( OutputPage $out, Skin $skin ) {

		// skip if sharing buttons are not displayed in this namespace
		if ( false === self::$isAllowedNamespace ) {
			return true;
		}

		$out->addModuleStyles( 'SharePage' );

		return true;
	}

	/**
	 * Set up list of services and build sharing URLs
	 *
	 * @param      array  $services
	 *
	 * @return     bool   true
	 */
	private static function buildServiceList( array $services ) {

		global $wgSharePageComment, $wgOut;

		// filter input for known services
		foreach ( $services as $name ) {
			if ( isset( self::$knownServices[$name] ) ) {

				// replace place holder with page information
				$service = self::$knownServices[$name];
				$service['url'] = str_replace( "%url%", urlencode( $wgOut->getTitle()->getFullURL() ), $service['url'] );
				$service['url'] = str_replace( "%comment%", urlencode( $wgSharePageComment ), $service['url'] );
				$service['url'] = str_replace( "%raw-comment%", $wgSharePageComment, $service['url'] );
				$service['url'] = str_replace( "%raw-url%", $wgOut->getTitle()->getFullURL(), $service['url'] );

				// add to list of displayed services
				self::$myServices[$name] = $service;
			}
		}

		return true;
	}

	/**
	 * Add a button for one specific social sharing service
	 *
	 * @param      string  $name
	 * @param      array   $service
	 *
	 * @return     string  $html
	 */
	private static function addShareButton( string $name, array $service ) {

		global $wgSharePageRoundedIcons;

		// set action dependent on url scheme
		list( $scheme, $location ) = explode( ':', $service['url'], 2 );

		switch ( $scheme ) {

			case 'javascript':
				$action = "onclick='$location'";
			break;

			default:
				$action = "href='${service['url']}' target='_blank'";
		}

		// use round icons if desired
		$css_round = $wgSharePageRoundedIcons ? 'sp-button-round' : '';

		// button html
		$html = "<li class='sp-button'>";
		$html .= "<i class='sp-button-background ${css_round}' style='background-color:${service['bg-color']}'>";
		$html .= "<a class='sp-icon sp-icon-${name} ${css_round}' ${action}>";
		$html .= '</a></i></li>';

		return $html;
	}
}
