# Sharepage

This extension adds social media buttons at the end of each article.

It's currently developed by [David Gruner](https://gruniversal.de) as part of the [ErWiN](https://gitlab.com/gruniversal/erwin/) project, but can be used separately as well.

The main idea and SVGs were taken from the great [Sassy Social Share](https://www.heateor.com/sassy-social-share/) plugin from [Heateor](https://www.heateor.com/). Thanks a lot!

This extension is also inspired by the [WikiShare](https://www.mediawiki.org/wiki/Extension:WikiShare) extension, which served as a good inspiration to start.

The source code is published under this open source license:
https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de

## Download

The original source location for this extension is:
https://gitlab.com/gruniversal/erwin/-/tree/master/src/extensions/SharePage/

You can download a zip of the latest version here:
https://gitlab.com/gruniversal/erwin/-/archive/master/erwin-master.zip?path=src/extensions/SharePage

## Installation

Download the extension and place the files in a directory called `SharePage` in your `extensions/` folder.

Add the following code at the bottom of your `LocalSettings.php`:

```php
wfLoadExtension( 'SharePage' );
```

Navigate to `Special:Version` on your wiki to verify that the extension is successfully installed.

## Usage

The extension automatically adds the social media buttons beneath each article.

## Configuration

Add any one of the following settings to your `LocalSettings.php` to customize the appearance of your sharing buttons.

**$wgSharePageServices** (array)
* definition and order of social media services
* see [`SharePage.php`](includes/SharePage.php) for list of supported services
* example: `$wgSharePageServices = [ "facebook", "twitter", "linkedin", "whatsapp", "email", "copylink" ];`
* note: since there is a [core bug](https://phabricator.wikimedia.org/T142663) in defining flat arrays the default value is currently empty
* update: with MW 1.35.3 / MW 1.36.1 the new merge strategy "provide_default" is available which could be used if the minimum version for this extension is raised accordingly
* default: `$wgSharePageServices = [];`

**$wgSharePageTitle** (string)
* this title is displayed above the sharing buttons
* default: `$wgSharePageTitle = "<b>If you like this page, you may share it:</b>";`

**$wgSharePageTextLeft** (string)
* this text is displayed left of the sharing buttons
* default: `$wgSharePageTextLeft = "";`

**$wgSharePageComment** (string)
* the comment is added to some services as additional information
* default: `$wgSharePageComment = "";`

**$wgSharePageRoundedIcons** (boolean)
* if set to true round buttons are used instead of squares
* default: `$wgSharePageRoundedIcons = false;`

**$wgSharePageAllowedNamespaces** (array)
* social sharing will only appear on pages within these namespaces (list of strings or ids)
* see also: https://www.mediawiki.org/wiki/Manual:Namespace
* default: `$wgSharePageAllowedNamespaces = [ "NS_MAIN" ];`

## General Data Protection Regulation

Since only links are displayed with this extension, there is no transfer of personal information before the user clicks on a particular social media service.

So this extension can be regarded fully compliant with the General Data Protection Regulation (GDPR).

## Wishlist

Here is an unsorted list of possible future development ideas:
* add more variables (eg. %wiki%, %longcomment%)
* add more options for positioning (eg. top, left/right, sticky)
* add via-parameter for twitter
* add more services
* add service names to configuration for tooltips (with translation)
