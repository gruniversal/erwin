function toggle_roadmap($id) {

	if ( $('#' + $id).attr('data-collapsed') == "true" ) {

		$('#' + $id).children('.roadmap-rows').css({
			'max-height' : '1600px'
		});

		$('#' + $id).children('.roadmap-title').children('.roadmap-title-toggle').css({
			'transform' : 'scale(1, -1)'
		});

		$('#' + $id).attr('data-collapsed', 'false');

	} else {

		$('#' + $id).children('.roadmap-rows').css({
			'max-height' : '0px'
		});

		$('#' + $id).children('.roadmap-title').children('.roadmap-title-toggle').css({
			'transform' : 'scale(1, 1)'
		});

		$('#' + $id).attr('data-collapsed', 'true');
	}
}

$( ".roadmap-title" ).on( {
	click: function() {
		toggle_roadmap(	$(this).parent().attr('id') );
	}
} );
