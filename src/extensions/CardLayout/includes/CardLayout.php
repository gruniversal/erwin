<?php

/**
 * This class adds parser tags to create a basic responsive card layout.
 *
 * @see: https://gitlab.com/gruniversal/erwin/-/tree/master/src/extensions/CardLayout/
 *
 * @license	https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author	David Gruner, https://gruniversal.de
 *
 */
class CardLayout {

	/**
	 * Insert stylesheet for responsive card layout
	 *
	 * @param      OutputPage  $out
	 * @param      Skin        $skin
	 *
	 * @return     true
	 */
	public static function onBeforePageDisplay( OutputPage $out, Skin $skin ) {

		$out->addModuleStyles( 'ext.CardLayout' );

		return true;
	}

	/**
	 * Register hook functions
	 *
	 * @param      Parser  $parser
	 *
	 * @return     true
	 */
	public static function onParserFirstCallInit( Parser $parser ) {

		// set classic hooks
		$parser->setHook( 'card-layout-begin', [ self::class, 'hookCardLayoutBegin'] );
		$parser->setHook( 'card',              [ self::class, 'hookCard'] );
		$parser->setHook( 'card-layout-end',   [ self::class, 'hookCardLayoutEnd'] );

		// set hooks for extension ParserFunctions (if present)
		if ( ExtensionRegistry::getInstance()->isLoaded( 'ParserFunctions' ) ) {
			$parser->setFunctionHook( 'card-layout-begin', [ self::class, 'hookFunctionCardLayoutBegin' ] );
			$parser->setFunctionHook( 'card',              [ self::class, 'hookFunctionCard' ] );
			$parser->setFunctionHook( 'card-layout-end',   [ self::class, 'hookFunctionCardLayoutEnd' ] );
			$parser->setFunctionHook( 'card-layout',       [ self::class, 'hookFunctionCardLayout' ] );
		}

		return true;
	}

	##
	## card-layout
	##

	// Hook for ParserFunction #card-layout
	public static function hookFunctionCardLayout( Parser $parser, $param ) {

		$html = self::renderCardLayoutBegin() . $param . self::renderCardLayoutEnd();

		return $html;
	}

	##
	## card-layout-begin
	##

	// Hook for parser tag <card-layout-begin>
	public static function hookCardLayoutBegin( $in, array $param, Parser $parser, PPFrame $frame ) {
		return self::renderCardLayoutBegin();
	}

	// Hook for ParserFunction #card-layout-begin
	public static function hookFunctionCardLayoutBegin( Parser $parser ) {
		return self::renderCardLayoutBegin();
	}

	public static function renderCardLayoutBegin() {

		$html = '
		<div class="cards">
  			<div class="cards_wrap">';

		return $html;
	}

	##
	## card-layout-end
	##

	// Hook for parser tag <card-layout-end>
	public static function hookCardLayoutEnd( $in, array $param, Parser $parser, PPFrame $frame ) {
		return self::renderCardLayoutEnd();
	}

	// Hook for ParserFunction #card-layout-end
	public static function hookFunctionCardLayoutEnd( Parser $parser ) {
		return self::renderCardLayoutEnd();
	}

	public static function renderCardLayoutEnd() {

		$html = '
		  </div>
		</div>';

		return $html;
	}

	##
	## card
	##

	// Hook for parser tag <card>
	public static function hookCard( $in, array $param, Parser $parser, PPFrame $frame ) {
		return self::renderCard( $in );
	}

	// Hook for ParserFunction #card
	public static function hookFunctionCard( Parser $parser, string $param1, string $param2 = '' ) {

		// if there is only one parameter, use it as $content
		if ( '' === $param2 ) {
			return self::renderCard( $param1 );
		}

		return self::renderCardWithTitle( $param1, $param2 );
	}

	/**
	 * Render HTML for a card without title
	 *
	 * @param      string  $content
	 *
	 * @return     string  html
	 */
	public static function renderCard( string $content ) {

		$html = '
		<div class="card">
		  <div class="card_inner">
		    <div class="card_content">' . "\n" . $content . '</div>
		  </div>
		</div>';

		return $html;
	}

	/**
	 * Render HTML for a card with title
	 *
	 * @param      string  $title
	 * @param      string  $content
	 *
	 * @return     string  html
	 */
	public static function renderCardWithTitle( string $title, string $content ) {

		$html = '
		<div class="card">
		  <div class="card_inner">
		    <div class="card_title">' . "\n" . $title . '</div>
		    <div class="card_content">' . "\n" . $content . '</div>
		  </div>
		</div>';

		return $html;
	}

}
