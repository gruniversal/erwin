<?php

##
## Self Check
##
## This script can be used to check your ErWiN application.
##

# enable self-check (use only temporary to prevent information disclosure)
$self_check_enabled = false;

if ( true !== $self_check_enabled ) {
	print '<pre>disabled';
	exit(1);
}

# this array holds the information
$sc = [
	'skins' => [],
	'extensions' => []
];

function wfLoadSkin( $skin ) {
	global $sc;

	$sc['skins'][] = $skin;
}

function wfLoadExtension( $extension ) {
	global $sc;

	$sc['extensions'][] = $extension;
}

function wfLoadExtensions( $list ) {
	global $sc;

	foreach ( $list as $extension ) {
		$sc['extensions'][] = $extension;
	}
}

function require_file( $file ) {
	global $sc;

	$sc['require'][] = $file;
}

# needed to skip entry prevention
define('MEDIAWIKI', 'self-check');

# include the actual settings
require_once('ErWiNSettings.php');
require_once('../LocalSettings.php');

# some html stuff
$ok = '<span style="color: #0c0">&#10004;</span>';
$xx = '<span style="color: #c00">&#10008;</span>';

print "<pre>";
print "*** ErWiN Self Check ***<br>";
print "<br>";
print "<b>Server:</b> $wgServer<br>";
$server = ( $_SERVER['HTTPS'] ? "https://" : "http://" ) . $_SERVER['SERVER_NAME'];
if ( $server === $wgServer ) {
	print "$ok configuration server name matches current server name<br>";
} else {
	print "$xx configuration server name mismatches current server name: $server<br>";
}

# check php version
if ( true === version_compare( PHP_VERSION, '7.3.19', '>=' ) ) {
	print "$ok php >= 7.3.19: " . PHP_VERSION . "<br>";
}

# check php extensions
$loaded_ext = get_loaded_extensions();
$mandatory_ext = [
	'ctype',
	'dom',
	'fileinfo',
	'iconv',
	'json',
	'mbstring',
	'mysqli',
	'pcre',
	'SPL',
	'xml'
];

$mandatory_ext_missing = 0;
foreach ( $mandatory_ext as $extension ) {
	if ( false === in_array( $extension, $loaded_ext ) ) {
		print "$xx mandatory php extension $extension not found<br>";
		$mandatory_ext_missing += 1;
	}
}
if ( 0 === $mandatory_ext_missing ) {
	print "$ok all mandatory php extensions found<br>";
}
if ( ini_get( 'apc.enabled' ) && extension_loaded( 'apcu' ) ) {
	print "$ok apcu support detected<br>";
	// activating CACHE_ACCEL blocks admin login for an unknown reason
	// if ( 'CACHE_ACCEL' !== $wgMainCacheType ) {
	//     print "$xx set \$wgMainCacheType to CACHE_ACCEL to increase site performance<br>";
	// }
}

# check if imagemagick and diff3 are present
if ( is_executable( $wgImageMagickConvertCommand ) ) {
	$output = shell_exec( $wgImageMagickConvertCommand . " --version");
	$line = explode( "\n", $output, 2 );
	print "$ok $wgImageMagickConvertCommand: $line[0]<br>";
} else {
	print "$xx $wgImageMagickConvertCommand is not executable<br>";
}
if ( is_executable( $wgDiff3 ) ) {
	$output = shell_exec($wgDiff3 . " --version");
	$line = explode( "\n", $output, 2 );
	print "$ok $wgDiff3: $line[0]<br>";
} else {
	print "$xx $wgDiff3 is not executable<br>";
}

# session handling
if ( "0" == ini_get( 'session.auto_start' ) ) {
	print "$ok session.auto_start: " . ini_get( 'session.auto_start' ) . "<br>";
} else {
	print "$xx session.auto_start: " . ini_get( 'session.auto_start' ) . " should be set to '0'<br>";
}
if ( is_writable( ini_get( 'session.save_path' ) ) ) {
	print "$ok session.save_path: " . ini_get( 'session.save_path' ) . " is writeable<br>";
} else {
	print "$xx session.save_path: " . ini_get( 'session.save_path' ) . " is not writeable<br>";
}
if ( ( ini_get('session.gc_maxlifetime') / 60 ) > 20 ) {
	print "$ok session.gc_maxlifetime: " . ( ini_get( 'session.gc_maxlifetime' ) / 60 ) . " minutes <br>";
} else {
	print "$xx session.gc_maxlifetime: " . ( ini_get( 'session.gc_maxlifetime' ) / 60 ) . " minutes <br>";
}

print "<br>";

print "<b>Cookies:</b><br>";
print "expiration:   " . ( $wgCookieExpiration / 86400) . " days<br>";
print "login cookie: " . ( $wgExtendedLoginCookieExpiration / 86400) . " days<br>";
print "<br>";

print "<b>Cache:</b><br>";
print "main:     $wgMainCacheType<br>";
print "session:  $wgSessionCacheType<br>";
print "parser:   $wgParserCacheType<br>";
print "language: $wgLanguageConverterCacheType<br>";
print "message:  $wgMessageCacheType<br>";
print "<br>";

# check if database is there and not empty
print "<b>Database:</b> $wgDBname at $wgDBserver ($wgDBtype)<br>";

$db = new mysqli( $wgDBserver, $wgDBuser, $wgDBpassword, $wgDBname );
if ( $db->connect_error ) {
	print "$xx connection to database failed: check database configuration<br>";
} else {
	$result = $db->query("SELECT CONCAT(sum(ROUND(((DATA_LENGTH/1024 + INDEX_LENGTH/1024) / 1024),2)),' MB') AS Size FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = '$wgDBname';");
	$dbsize = $result->fetch_row();
	$result = $db->query("SELECT table_name, table_rows FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$wgDBname';");
	$dbschema = $result->fetch_all(MYSQLI_ASSOC);
	$dbtables = [];
	foreach ( $dbschema as $entry ) {
		$dbtables[$entry['table_name']] = $entry['table_rows'];
	}
	$dbcount = count( $dbtables );
	print "$ok connected as user $wgDBuser to a $dbsize[0] database with $dbcount tables<br>";

	if (0 === $dbcount) {
		print "$xx database is empty: you need to import a database dump<br>";
	}

	if ( false === isset( $dbtables[$wgDBprefix . 'logging'] ) ) {
		print "$xx table ${wgDBprefix}logging is not found: check database scheme<br>";
	} else {
		if ( 0 == $dbtables[$wgDBprefix . 'logging'] ) {
			print "$ok table ${wgDBprefix}logging found (empty)<br>";
		} else {
			$result = $db->query("SELECT `log_timestamp` FROM `${wgDBprefix}logging` LIMIT 1");
			$row = $result->fetch_row();
			print "$ok $row[0] - first timestamp in table ${wgDBprefix}logging<br>";
		}
	}

	if ( false === isset( $dbtables[$wgDBprefix . 'recentchanges'] ) ) {
		print "$xx table ${wgDBprefix}recentchanges is not found: check database scheme<br>";
	} else {
		if ( 0 == $dbtables[$wgDBprefix . 'recentchanges'] ) {
			print "$ok table ${wgDBprefix}recentchanges found (empty)<br>";
		} else {
			$result = $db->query("SELECT MAX(`rc_timestamp`) FROM `${wgDBprefix}recentchanges`");
			$row = $result->fetch_row();
			print "$ok $row[0] - last timestamp in table ${wgDBprefix}recentchanges<br>";
		}
	}

	$db->close();
}

print "<br>";

print "<b>Mediawiki:</b> $wgSitename<br>";

foreach ( $sc['skins'] as $skin ) {
	print "$ok skin $skin<br>";
}
foreach ( $sc['extensions'] as $extension ) {
	print "$ok extension $extension<br>";
}
foreach ( $sc['require'] as $require ) {
	print "$ok require $require<br>";
}
