<?php

##
## ErWiN settings
##
## This configuration file contains all basic ErWiN settings.
##
## All settings here can be overwritten in your local settings file.
##
## You should not make any changes here. Use LocalSettings.php instead.
##

# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

### BASIC CONFIGURATION ###

## site name
$wgSitename         = "ErWiN";
$wgMetaNamespace    = "ErWiN";

## The protocol and server name to use in fully-qualified URLs
$wgServer = "https://erwin.employeesforfuture.mars";

## The URL base path to the directory containing the wiki;
## defaults for all runtime URL paths are based off of this.
## For more information on customizing the URLs
## (like /w/index.php/Page_title to /wiki/Page_title) please see:
## https://www.mediawiki.org/wiki/Manual:Short_URL
$wgScriptPath = "";

## The URL path to static resources (images, scripts, etc.)
$wgResourceBasePath = $wgScriptPath;

## The configuration of the logo files (from Dryad skin)
$wgLogos = [
	'1x' => "$wgScriptPath/skins/Dryad/resources/E4F_logo_135x135.png",
	'2x' => "$wgScriptPath/skins/Dryad/resources/E4F_logo_270x270.png",
	'wordmark' => [
		'src' => "$wgScriptPath/skins/Dryad/resources/ErWiN-logo_RGB_left-fff_cut_270_59.png",
		'width' => 135,
		'height' => 32,
	]
];

## Set favicon and Apple touch icon as well
$wgFavicon = "$wgScriptPath/skins/Dryad/resources/favicon.ico";
$wgAppleTouchIcon = "$wgScriptPath/skins/Dryad/resources/apple-touch-icon.png";

## Redirect insecure HTTP requests to HTTPS.
$wgForceHTTPS = true;

### LICENSE ###

## For attaching licensing metadata to pages, and displaying an
## appropriate copyright notice / icon. GNU Free Documentation
## License and Creative Commons licenses are supported so far.
$wgRightsPage = "$wgMetaNamespace:Urheberrechte"; # Set to title of a wiki page that describes your license/copyright
$wgRightsUrl  = "https://creativecommons.org/licenses/by-sa/4.0/";
$wgRightsText = "Creative Commons „Namensnennung – Weitergabe unter gleichen Bedingungen“";
$wgRightsIcon = "$wgResourceBasePath/resources/assets/licenses/cc-by-sa.png";

### DATABASE ###

## Database settings
$wgDBtype       = "mysql";
$wgDBserver     = "localhost";
$wgDBname       = "mediawiki";
$wgDBuser       = "mediawiki";
$wgDBpassword   = "mediawiki";

## prefix table-names when using a shared database
$wgDBprefix = "";

## MySQL table options to use during installation or update
$wgDBTableOptions = "ENGINE=InnoDB, DEFAULT CHARSET=binary";

### CACHE SETTINGS ###

## Caching is needed for the login captcha (Extension: ConfirmEdit) but
## also tricky, because caching in the database can slow down the site.
## If available we use the apc extension instead of the database, which
## the increases memory consumption of the application.
##
## @see: https://www.mediawiki.org/wiki/Manual:$wgMainCacheType
## @see: https://www.mediawiki.org/wiki/Manual:Performance_tuning#Object_caching

# this tells mediawiki to use the database for caching
$wgMainCacheType     = CACHE_DB;

// since we had some trouble with session caching, we now set everything to CACHE_DB
// as is it said to be the most safe option -- hope it helps
$wgSessionCacheType  = CACHE_DB;
$wgParserCacheType   = CACHE_DB;
$wgLanguageConverterCacheType   = CACHE_DB;
$wgMessageCacheType  = CACHE_DB;

# this seems to be to the minimum requirement for the login captcha:
// $wgMainCacheType     = CACHE_DB;
// $wgSessionCacheType  = CACHE_DB;
// $wgParserCacheType   = CACHE_NONE;

# if apc is present use it
// disabled to ensure same setup for local and live (apc is not available in live)
// if ( ini_get('apc.enabled') && extension_loaded('apcu') ) {
//	//$wgMainCacheType    = CACHE_ACCEL; // admin login does not work (unknown reason)
//	$wgSessionCacheType = CACHE_ACCEL;
//	$wgParserCacheType  = CACHE_ACCEL;
//	$wgLanguageConverterCacheType   = CACHE_ACCEL;
//	$wgMessageCacheType = CACHE_ACCEL;
// }

## shared memory is a good idea for bigger sites - disabled by default
## @see: https://www.mediawiki.org/wiki/Manual:Memcached
$wgMemCachedServers = [];

### COOKIE SETTINGS ###

## set a cookie prefix to prevent using the database name
$wgCookiePrefix = "erwin_";

## use long time cookies
$wgCookieExpiration = 90 * 86400;
$wgExtendedLoginCookieExpiration = 90 * 86400;

## restrict use of login cookies to same site (see: https://phabricator.wikimedia.org/T255366)
$wgCookieSameSite = "Strict";

### MAIL CONFIGURATION ###

## mail address for the site
$wgEmergencyContact = "erwin@employeesforfuture.mars";
$wgPasswordSender   = "erwin@employeesforfuture.mars";

## enable mailing, but disable user-to-user mailings
$wgEnableEmail          = true;
$wgEnableUserEmail      = true; # this is also a user preference option
$wgEnotifUserTalk       = false; # this is also a user preference option
$wgEnotifWatchlist      = true; # this is also a user preference option
$wgEmailAuthentication  = true;

### MEDIA AND UPLOADS ###

## To enable image uploads, make sure the 'images' directory is writable
$wgEnableUploads = true;

## imagemagick
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";

## InstantCommons allows wiki to use images from https://commons.wikimedia.org
$wgUseInstantCommons = false;

## Periodically send a pingback to https://www.mediawiki.org/ with basic data
## about this MediaWiki instance. The Wikimedia Foundation shares this data
## with MediaWiki developers to help guide future development efforts.
$wgPingback = false;

### SYSTEM CONFIGURATION ###

## Site language code, should be one of the list in ./languages/data/Names.php
$wgLanguageCode = "de";

## If you use ImageMagick (or any other shell command) on a
## Linux server, this will need to be set to the name of an
## available UTF-8 locale
$wgShellLocale = "C.UTF-8";

## Set $wgCacheDirectory to a writable directory on the web server
## to make your wiki go slightly faster. The directory should not
## be publicly accessible from the web.
#$wgCacheDirectory = "$IP/cache";

## Path to the GNU diff3 utility. Used for conflict resolution.
$wgDiff3 = "/usr/bin/diff3";

## Limit http connect and request times (see: https://phabricator.wikimedia.org/T245170)
$wgHTTPMaxConnectTimeout = 10;  // 10 seconds
$wgHTTPMaxTimeout = 10 * 60;    // 10 minutes

## Disable Parser Limit Report
$wgEnableParserLimitReporting = false;

### SECURITY SETTINGS ###

## secret key: this should be unique for your site!
## your can generate by uncommenting the following line
// print MWCryptRand::generateHex( 64, true ); exit;
## @see: https://www.mediawiki.org/wiki/Manual:$wgSecretKey
$wgSecretKey = "0c5c6f2c2423483f342660d1b9359792732567ef7e81ba5142578cdca811d9c5";

## Changing this will log out all existing sessions.
$wgAuthenticationTokenVersion = "1";

## Site upgrade key. Must be set to a string (default provided) to turn on the
## web installer while LocalSettings.php is in place
$wgUpgradeKey = "e887990cb985ac07";

## Disable user-specific CSS and JS files
$wgAllowUserCss = false;
$wgAllowUserJs = false;

## Disable write access to the API
$wgEnableWriteAPI = false;

## Use only files for messages (speeds up a bit and allows for changes to be released)
$wgUseDatabaseMessages = false;

## Set some password policies
$wgPasswordPolicy['policies']['default']['MinimalPasswordLength'] = 8;
$wgPasswordPolicy['policies']['default']['MaximalPasswordLength'] = 32;
// deprecated: $wgPasswordPolicy['policies']['default']['PasswordCannotBePopular'] = 100;
$wgPasswordPolicy['policies']['default']['PasswordNotInCommonList'] = true;
$wgPasswordPolicy['policies']['default']['PasswordCannotMatchUsername'] = true;

$wgPasswordPolicy['policies']['bureaucrat']['MinimalPasswordLength'] = 8;
$wgPasswordPolicy['policies']['sysop']['MinimalPasswordLength'] = 16;

## Disable jquery migrate (will be obsolete with MW 1.38, see https://phabricator.wikimedia.org/T280944)
## Update: WikiEditor does not work if disabled so re-enabled until it is fixed (see https://phabricator.wikimedia.org/T213426)
$wgIncludejQueryMigrate = true;

### HOOKS ###

## Check the password "remember me" box by default (to help prevent unwanted logout)
## @see: https://www.mediawiki.org/wiki/User:Ammarpad/sandbox
$wgHooks['AuthChangeFormFields'][] = function (
	$requests, $fieldInfo, &$formDescriptor, $action
) {
	$formDescriptor['rememberMe'] = [
		'type' => 'check',
		'default' => true,
		'label-message' => "userlogin-remembermypassword"
	];
	return true;
};

### PRIVILEGES ###

## For security reasons we strictly limit user rights
## to what is needed on this site (whitelisting).
##
## @see: https://erwin.employeesforfuture.mars/index.php/Spezial:Gruppenrechte
## @see: https://de.wikipedia.org/wiki/Hilfe:Benutzer
## @see: https://www.mediawiki.org/wiki/Manual:$wgGroupPermissions#Custom_user_groups

# Disable all privileges for bots -- we don't use them at the moment
$wgGroupPermissions['bot'] = [];

# Disable all privileges for interface admins -- we don't use them at the moment
$wgGroupPermissions['interface-admin'] = [];

# Disable all privileges for suppressors -- we don't use them at the moment
$wgGroupPermissions['suppress'] = [];

# Allow only these privileges for anonymous users (whitelisting)
$wgGroupPermissions['*'] = [
	'read'                  => true
];

# Allow only these privileges for regular users (whitelisting)
$wgGroupPermissions['user'] = [
	'read'                  => true,
	'edit'                  => true,
	'minoredit'             => true,
	'createpage'            => true,
	'createtalk'            => true,
	'changetags'            => true,
	'applychangetags'       => true,
	'purge'                 => true,
	'viewmywatchlist'       => true,
	'editmywatchlist'       => true,
	'editmyoptions'         => true,
	'viewmyprivateinfo'     => true,
	'editmyprivateinfo'     => true
];

# Users run through a test period of 14 days (before we call them "Newbies")
$wgAutoConfirmAge = 14 * 86400;

# These rights are granted after the test period
$wgGroupPermissions['autoconfirmed'] = [
	'autoconfirmed'         => true,
	'editsemiprotected'     => true,
	'move'                  => true,
	'move-categorypages'    => true,
	'move-subpages'         => true,
	'move-rootuserpages'    => true
];

# Add a special group for approvers / quality guards
$wgGroupPermissions['quality-guard'] = [
	'approverevisions'      => true,
	'viewlinktolatest'      => true,
	'viewapprover'          => true,
	'move'                  => true,
	'move-categorypages'    => true,
	'move-subpages'         => true,
	'move-rootuserpages'    => true,
	'delete'                => true,
	'deletedhistory'        => true,
	'deletedtext'           => true,
	'browsearchive'         => true,
	'undelete'              => true
];

# Add a special group for uploads / filemanagers (potential security risk)
$wgGroupPermissions['file-manager'] = [
	'upload'                => true,
	'reupload'              => true,
	'reupload-shared'       => true,
	'movefile'              => true
];

# Disallow bureaucrats to disable admins
$wgGroupPermissions['sysop']['userrights'] = true;
$wgGroupPermissions['bureaucrat']['userrights'] = false;
$wgAddGroups['bureaucrat']      = [ 'quality-guard', 'file-manager', 'bureaucrat' ];
$wgRemoveGroups['bureaucrat']   = [ 'quality-guard', 'file-manager', 'bureaucrat' ];

# Allow admins and bureaucrats to send emails
$wgGroupPermissions['sysop']['sendemail'] = true;
$wgGroupPermissions['bureaucrat']['sendemail'] = true;

# Allow API requests only to these groups
$wgGroupPermissions['sysop']['writeapi'] = true;
$wgGroupPermissions['file-manager']['writeapi'] = true;

# limit pages in namespace ErWiN to users with approverevisions privilege (quality guards)
$wgNamespaceProtection[NS_PROJECT] = ['approverevisions'];

### FILE UPLOADS ###

## Uploaded files are critical in terms of security aspects.
##
## We limit uploads to only some hopefully untroubled formats.
##
## @see: https://www.mediawiki.org/wiki/Manual:Security#Upload_security
## @see: https://www.mediawiki.org/wiki/Manual:$wgFileExtensions
## @see: https://www.mediawiki.org/wiki/Manual:Configuring_file_uploads/de

$wgFileExtensions = [ 'png', 'gif', 'jpg', 'jpeg', 'webp', 'pdf' ];

### PARSER SETTINGS ###

# open external links in new tab
$wgExternalLinkTarget = '_blank';

### USER PREFERENCES ###

## Set some default values and disable user-specific preferences
## @see: https://www.mediawiki.org/wiki/Manual:$wgDefaultUserOptions#Available_preferences

# users should not change the skin
$wgHiddenPrefs[] = 'skin';

# ErWiN is only available in german (at least for now)
$wgHiddenPrefs[] = 'language';

# we don't need fancy signature stuff
$wgHiddenPrefs[] = 'fancysig';

# gender does not matter - we are all human
$wgHiddenPrefs[] = 'gender';

# use standard for date and timezone
$wgHiddenPrefs[] = 'date';
$wgHiddenPrefs[] = 'timecorrection';

# underline is controlled by theme
$wgHiddenPrefs[] = 'underline';

# probably unused features
$wgHiddenPrefs[] = 'stubthreshold';
$wgHiddenPrefs[] = 'showhiddencats';
$wgHiddenPrefs[] = 'numberheadings';

# editing
$wgHiddenPrefs[] = 'editsectiononrightclick';
$wgHiddenPrefs[] = 'editondblclick';

# preview below editing and without reload
$wgDefaultUserOptions['previewontop'] = 0;
$wgDefaultUserOptions['uselivepreview'] = 1;

# recent changes
$wgDefaultUserOptions['rcdays'] = 90;
$wgDefaultUserOptions['hideminor'] = 1;

# watchlist
$wgDefaultUserOptions['watchlistdays'] = 90;
$wgHiddenPrefs[] = 'watchlistdays';
$wgHiddenPrefs[] = 'watchlistunwatchlinks';
$wgHiddenPrefs[] = 'watchlisthidebots';
$wgHiddenPrefs[] = 'watchlisthideanons';
$wgHiddenPrefs[] = 'watchlisthideliu';

### SKINS ###

## Default skin
$wgDefaultSkin = "dryad";

## Enabled skins

# Timeless is needed for Dryad to run
wfLoadSkin( 'Timeless' );
wfLoadSkin( 'Dryad' );

# replace cat.svg with some sign of love :)
$wgTimelessBackdropImage = "images/heart-grey.svg";

### EXTENSIONS ###

## WikiEditor greatly improves editing wiki markup
wfLoadExtension( 'WikiEditor' );

## MsUpload simplifies file uploads
wfLoadExtension( 'MsUpload' );

## ConfirmEdit adds Captchas to prevent brute-force attacks
wfLoadExtensions([ 'ConfirmEdit', 'ConfirmEdit/QuestyCaptcha' ]);

# add a captcha for some actions
# @see: https://www.mediawiki.org/wiki/Extension:ConfirmEdit#Configuration
$wgCaptchaTriggers['edit']          = false;
$wgCaptchaTriggers['create']        = false;
$wgCaptchaTriggers['createtalk']    = false;
$wgCaptchaTriggers['addurl']        = false;
$wgCaptchaTriggers['createaccount'] = true;
$wgCaptchaTriggers['badlogin']      = true;

# some questions bots don't understand, but humans can answer without thinking
$wgCaptchaQuestions = [
	'<b>CAPTCHA:</b> Wie heißt die Klimaaktivistin Thunberg mit Vornamen?' => 'Greta',
	'<b>CAPTCHA:</b> Was wird umgangssprachlich auch als "Drahtesel" bezeichnet?'
		=> ['Fahrrad','das Fahrrad','ein Fahrrad'],
	'<b>CAPTCHA:</b> Wie nennt man Strom, der ausschließlich aus nachhaltigen Energiequellen gewonnen wird?'
		=> ['Ökostrom', 'Öko-Strom', 'grüner Strom', 'grünen Strom'],
	'<b>CAPTCHA:</b> Wofür steht das N in ErWiN?' => ['Nachhaltigkeit'],
	'<b>CAPTCHA:</b> Welches ist das Fragewort dieser Frage?' => ['Welches']
];

# captchas can not be skipped
$wgGroupPermissions['bot']['skipcaptcha'] = false;
$wgGroupPermissions['sysop']['skipcaptcha'] = false;

## ConfirmAccount is used for user registration process
## @see: https://www.mediawiki.org/wiki/Extension:ConfirmAccount
## @see: vendor/extensions/ConfirmAccount/ConfirmAccount.config.php
wfLoadExtension( 'ConfirmAccount' );
$wgConfirmAccountRequestFormItems = [
	'UserName'        => [ 'enabled' => true ],
	'RealName'        => [ 'enabled' => false ],
	'Biography'       => [ 'enabled' => true, 'minWords' => 30 ],
	'AreasOfInterest' => [ 'enabled' => false ],
	'CV'              => [ 'enabled' => false ],
	'Notes'           => [ 'enabled' => true ],
	'Links'           => [ 'enabled' => true ],
	'TermsOfService'  => [ 'enabled' => false ],
];

# notify erwin owner(s) to confirm user accounts
$wgConfirmAccountContact = "erwin@employeesforfuture.mars";

# notify all bureaucrats to confirm user accounts
$wgGroupPermissions['bureaucrat']['confirmaccount-notify'] = true;

# bureaucrats must be allowed to create accounts after confirmation
$wgGroupPermissions['bureaucrat']['createaccount'] = true;

# disable default mail authentication, since mail is already authenticated in registration process
$wgEmailAuthentication = false;

# store user notes and credentials only for registration process (data protection)
$wgConfirmAccountSaveInfo = false;

# allow up to 5 account requests per IP at once (company users may share one external IP)
$wgAccountRequestThrottle = 5;

# store rejected requests for one week
$wgRejectedAccountMaxAge = 7 * 24 * 3600;

# requests are considered rejected after two weeks (if not confirmed)
$wgConfirmAccountRejectAge = 14 * 24 * 3600;

## ApprovedRevs offers a quality assurance process for pages/files
wfLoadExtension( 'ApprovedRevs' );

# users should / visitors should not see, whether there is a newer version
$wgGroupPermissions['*']['viewlinktolatest'] = false;
$wgGroupPermissions['user']['viewlinktolatest'] = true;

# this prevents unapproved pages from being displayed (to the public)
$egApprovedRevsBlankIfUnapproved = true;

# set a custom blank page
# (only works with patched version of ApprovedRevs at the moment)
# (see: src/patches/0002_ApprovedRevs_configure_BlankPage.patch)
$egApprovedRevsBlankPage = 'ErWiN:Seite_in_Arbeit';

# disable automatic approvals for users with "approverevisions" privilege
$egApprovedRevsAutomaticApprovals = false;

# user pages don't need to be approved
$egApprovedRevsEnabledNamespaces[NS_USER] = false;
$egApprovedRevsEnabledNamespaces[NS_USER_TALK] = false;

## OpenGraphMeta adds support for og-tags (facebook, twitter, etc.)
wfLoadExtension( 'OpenGraphMeta' );

## Description2 adds support for og:description and makes it customizable via <metadesc>...</metadesc>
wfLoadExtension( 'Description2' );
$wgEnableMetaDescriptionFunctions = true;

## PageImages adds images information to pages (used in OpenGraphMeta, etc.)
wfLoadExtension( 'PageImages' );

## CardLayout adds support for a basic responsive three-column card layout (used e.g. at main page)
wfLoadExtension( 'ParserFunctions' );
wfLoadExtension( 'CardLayout' );

## DynamicPageList adds support for dynamic page lists :D
wfLoadExtension( 'DynamicPageList' );

## Cite adds support for references <ref>
wfLoadExtension( 'Cite' );

## Matomo adds user tracking (see: README.md#matomo-tracking)
if ( $erwinOptionMatomo == true ) {
	wfLoadExtension( 'MatomoOptOut' );
	$wgMatomoURL = $wgServer . '/matomo/matomo.php';
	$wgMatomoIDSite = 1;
	$wgMatomoDisableCookies = true;
	$wgMatomoCustomJS = [
		'_paq.push(["enableHeartBeatTimer", 60]);'
	];
	$wgMatomoIgnoreEditors = true; // do not track users with login
	$wgMatomoIgnoreBots = true;
	$wgMatomoIgnoreSysops = true;
	$wgMatomoTrackUsernames = false;
}

## SharePage adds social media buttons to each article page
## @see: https://gitlab.com/gruniversal/erwin/-/tree/master/src/extensions/SharePage/
wfLoadExtension( 'SharePage' );

$wgSharePageServices = [
	'facebook',
	'twitter',
	'linkedin',
	'whatsapp',
	'telegram',
	'threema',
	'email',
	'copylink'
];

$wgSharePageTitle = '<b>Du fandest diesen Beitrag nützlich? Dann jetzt teilen!</b>';
$wgSharePageTextLeft = 'Teilen:';
$wgSharePageComment = 'Schau dir diesen Beitrag im #ErWiN an: ';

$wgSharePageRoundedIcons = true;

$wgSharePageAllowedNamespaces = [ NS_MAIN, NS_PROJECT ];

## ErwinRoadmap displays the E4F-Roadmap info box
wfLoadExtension( 'ErwinRoadmap' );
