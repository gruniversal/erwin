<?php

##
## Local settings
##
## This configuration file imports all basic ErWiN settings and
## overwrites them, if a site specific setting is needed.
##
## Your local settings are not part of the version control
## and therefore not overwritten in future releases.
##
## Your can also use this file to configure settings that
## vary between development and productive environments.
##
## See ErWiNSettings.php for basic configuration.
##
## In short: do all settings here, not everywhere else :)
##
## Further documentation for configuration settings may be found at:
## https://www.mediawiki.org/wiki/Manual:Configuration_settings

### OPTIONS ###

## enable matomo tracking
$erwinOptionMatomo = true;

### DEFAULT SETTINGS ###

## import default ErWiN settings
require_once('erwin/ErWiNSettings.php');

### LOCAL SETTINGS ###

## The protocol and server name to use in fully-qualified URLs
$wgServer = "https://erwin.employeesforfuture.mars";

## Database settings
$wgDBtype       = "mysql";
$wgDBserver     = "localhost";
$wgDBname       = "mediawiki";
$wgDBuser       = "mediawiki";
$wgDBpassword   = "mediawiki";

## mail address for the site
$wgEmergencyContact = "erwin@employeesforfuture.mars";
$wgPasswordSender   = "erwin@employeesforfuture.mars";

## secret key: this should be unique for your site!
## your can generate it by uncommenting the following line:
// print MWCryptRand::generateHex( 64, true ); exit;
## @see: https://www.mediawiki.org/wiki/Manual:$wgSecretKey
$wgSecretKey = "0c5c6f2c2423483f342660d1b9359792732567ef7e81ba5142578cdca811d9c5";

## enable Short-URLs on root level: e.g. https://erwin.employeesforfuture.mars/Hauptseite
## copy src/rewrite/.htaccess to your root directory first
$wgArticlePath = "/$1";

## notify erwin owner(s) to confirm user accounts
$wgConfirmAccountContact = "erwin@employeesforfuture.mars";

## un-comment to enable debug log
#$wgDebugLogFile = "/tmp/erwin-debug.log";

## enable for debugging in DEV environments
$wgShowExceptionDetails = true;

## upgrade key: this should be unique for your site!
## this has to been entered before performing upgrades via mw-config
## your can generate it by uncommenting the following line:
// print MWCryptRand::generateHex( 16, true ); exit;
## @see: https://www.mediawiki.org/wiki/Manual:$wgUpgradeKey
$wgUpgradeKey = "3e04706020df06f7";

## google site verification: un-comment this to add verification meta tag for google search console
// $wgHooks['OutputPageParserOutput'][] = function( OutputPage &$out, ParserOutput $parseroutput ) {
//     $out->addMeta( 'google-site-verification', '' );
//     return true;
// };

## Matomo settings
$wgMatomoURL = $wgServer . '/matomo/matomo.php';
$wgMatomoIDSite = 1;

