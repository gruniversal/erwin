# ErwinRoadmap

ErwinRoadmap is a [MediaWiki](https://www.mediawiki.org/) extension.

Author:
* David Gruner, https://www.gruniversal.de

License:
* https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

## What it does

It adds a parser function 'erwin-roadmap' that creates the [Employees for Future](https://www.employeesforfuture.org) roadmap info box.

## Usage

Add the following text to your wikitext:

	{{#erwin-roadmap:
		type|
		title|
		level1|
		level2|
		level3|
		level4
	}}

The `type` has to be set to a color: `yellow`, `teal`, `violet`, `light-green`, `dark-green`, `blue` or `gray` (default).

You may also use the names of the six different dimensions found in the E4F-Roadmap or any text that contains these words:
* Energie
* Mobilität
* Ressourcen
* Ernährung
* Biodiversität
* Wirkung

All other parameters are just text that is displayed in the roadmap info block.

Further information can be found in the code.
