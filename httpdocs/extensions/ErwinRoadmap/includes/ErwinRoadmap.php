<?php

/**
 * ErwinRoadmap
 *
 * @see: https://gitlab.com/gruniversal/erwin/-/tree/master/src/extensions/ErwinRoadmap/
 *
 * @license	https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author	David Gruner, https://gruniversal.de
 *
 */
class ErwinRoadmap {

	/**
	 * Insert stylesheet for ErwinRoadmap
	 *
	 * @param      OutputPage  $out
	 * @param      Skin        $skin
	 *
	 * @return     true
	 */
	public static function onBeforePageDisplay( OutputPage $out, Skin $skin ) {

		$out->addModuleStyles( 'ext.ErwinRoadmap.styles' );
		$out->addModules( 'ext.ErwinRoadmap.scripts' );

		return true;
	}

	/**
	 * Register hook functions
	 *
	 * @param      Parser  $parser
	 *
	 * @return     true
	 */
	public static function onParserFirstCallInit( Parser $parser ) {

		// set hooks for extension ParserFunctions (if present)
		if ( ExtensionRegistry::getInstance()->isLoaded( 'ParserFunctions' ) ) {
			$parser->setFunctionHook( 'erwin-roadmap', [ self::class, 'hookFunctionErwinRoadmap' ] );
		}

		return true;
	}

	/**
	 * Hook for ParserFunction #erwin-roadmap
	 *
	 * @param      Parser  $parser
	 * @param      <type>  $param
	 *
	 * @return     string  html
	 */
	public static function hookFunctionErwinRoadmap( Parser $parser, $type, $title, $level1, $level2, $level3, $level4 ) {

		switch ( $type ) {

			case 'yellow':
			case strpos( $type, 'Energie'):
				$css = "roadmap-yellow";
				break;

			case 'teal':
			case strpos( $type, 'Mobilität'):
				$css = "roadmap-teal";
				break;

			case 'violet':
			case strpos( $type, 'Ressourcen'):
				$css = "roadmap-violet";
				break;

			case 'light-green':
			case strpos( $type, 'Ernährung'):
				$css = "roadmap-light-green";
				break;

			case 'dark-green':
			case strpos( $type, 'Biodiversität'):
				$css = "roadmap-dark-green";
				break;

			case 'blue':
			case strpos( $type, 'Wirkung'):
				$css = "roadmap-blue";
				break;

			default:
				$css = "roadmap-gray";
				break;
		}

		$html = self::renderRoadmap( $css, $title, $level1, $level2, $level3, $level4 );

		return $html;
	}

	/**
	 * Render HTML
	 *
	 * @param      string  $content
	 *
	 * @return     string  html
	 */
	public static function renderRoadmap( $css, $title, $level1, $level2, $level3, $level4 ) {

		$id = 'roadmap_' . random_int(1000, 9999);

		$html = '
		<div id="' . $id . '" class="roadmap ' . $css . '" data-collapsed="true">
			<div class="roadmap-title">
				<div class="roadmap-title-text">
					Dieser Artikel hilft euch bei der Umsetzung der Roadmap-Kachel<br>
					<b>' . $title . '</b>
				</div>
				<div class="roadmap-title-toggle">&nbsp;</div>
			</div>
			<div class="roadmap-rows">
				<div class="roadmap-row">
					<div class="roadmap-row-data">
						Bestandsaufnahme​<br><i>1 Punkt​</i>
					</div>
					<div class="roadmap-row-text">
						Beinhaltet die unternehmensinterne Recherche zum Status quo: Was ist euer Ausgangspunkt?<br>
						<b>' . $level1 . '</b>
					</div>
				</div>
				<div class="roadmap-row">
					<div class="roadmap-row-data">
						Zielstellung definiert & Prozess angestoßen<br><i>2-3 Punkte</i>
					</div>
					<div class="roadmap-row-text">
						Nach Bestandsaufnahme: Pläne mit Führungskräften abklären. Zusätzlich: kleines Projektteam gründen oder eine*n Zuständige*n für die weitere Bearbeitung finden.<br>
						<b>' . $level2 . '</b>
					</div>
				</div>
				<div class="roadmap-row">
					<div class="roadmap-row-data">
						Pilotprojekt durchgeführt​<br><i>3-5 Punkte</i>
					</div>
					<div class="roadmap-row-text">
						Lohnenswert bei großen Projekten: Testphase mit Evaluationszyklus (bspw. an nur einem Standort oder mit nur einer Abteilung). Lasst euch dabei das Ziel aber nicht ausreden! Versucht es vielmehr, konstruktiv zu erreichen.<br>
						<b>' . $level3 . '</b>
					</div>
				</div>
				<div class="roadmap-row">
					<div class="roadmap-row-data">
						Unternehmens&shy;weite Umsetzung<br><i>6-10 Punkte</i>
					</div>
					<div class="roadmap-row-text">
						Nach Feedback: Unternehmensweite Umsetzung. Die Maßnahme sollte weiterhin regelmäßig geprüft und Feedback weiter aufgenommen werden.​<br>
						<b>' . $level4 . '</b>
					</div>
				</div>
			</div>
		</div>';

		return $html;
	}

}
