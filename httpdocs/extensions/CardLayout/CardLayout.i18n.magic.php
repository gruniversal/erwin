<?php

// register magic words for CardLayout extension

$magicWords = [

'en' => [
	'card-layout-begin' => [ 0, 'card-layout-begin' ],
	'card-layout-end'   => [ 0, 'card-layout-end' ],
	'card'              => [ 0, 'card' ],
	'card-layout'       => [ 0, 'card-layout' ]
]

];
