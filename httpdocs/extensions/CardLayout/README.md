# CardLayout

CardLayout is a [MediaWiki](https://www.mediawiki.org/) extension, that adds parser tags to create a basic responsive card layout with three columns.

Author:
* David Gruner, https://www.gruniversal.de

License:
* https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

## What it does

It adds the following parser tags:

| parser tag        | description                                    |
| ----------------- | ---------------------------------------------- |
| card-layout-begin | begin a card layout section                    |
| card              | content for a card, can be used multiple times |
| card-layout-end   | end a card layout section                      |
| card-layout       | card layout section (includes begin and end) <br>only available with [ParserFunctions](https://www.mediawiki.org/wiki/Extension:ParserFunctions) extension  |

## Usage

You can add a card layout section like this:

	<card-layout-begin/>
	<card>...</card>
	<card>...</card>
	...
	<card-layout-end/>

With ParserFunctions extension enabled there is a more comfortable syntax:

	{{#card-layout:
	{{#card: content}}
	{{#card: title|content}}
	{{#card: title|
	multi<br>
	line<br>
	content
	}}
	...
	}}

## Remarks

The development is mainly based on these ideas:
 * https://www.mediawiki.org/wiki/Extension:Poem
 * https://codepen.io/RajRajeshDn/pen/qBEGeEp
 * https://www.mediawiki.org/wiki/Manual:Parser_functions
 * https://www.mediawiki.org/wiki/Manual:Extension.json/Schema#manifest_version
 * https://www.mediawiki.org/wiki/ResourceLoader/Developing_with_ResourceLoader

Further information can be found in the code.
