<?php

/**
 * Main class for the Dryad skin
 *
 * @ingroup Skins
 */
class SkinDryad extends SkinTimeless {

	/** @var string skin name */
	public $skinname  = 'dryad';

	/** @var string style name */
	public $stylename = 'Dryad';

	/** @var string template name */
	public $template  = 'DryadTemplate';

	/**
	 * Add Dryad skin
	 *
	 * @param      OutputPage  $out
	 */
	public function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );
		$out->addModuleStyles( "skins.dryad" );
	}

}
