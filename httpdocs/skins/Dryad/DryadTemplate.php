<?php
/**
 * BaseTemplate class for the Dryad skin
 *
 * @ingroup Skins
 */
class DryadTemplate extends TimelessTemplate {

	/**
	 * Generate list of tools for the menus/portlets
	 *
	 * @see        https://de.wikipedia.org/wiki/Wikipedia:Technik/Skin/GUI
	 *
	 * @return     array  $sortedPileofTools
	 */
	protected function getPageTools() {

		$user = $this->getSkin()->getUser();
		$sortedPileOfTools = parent::getPageTools();

// print "<pre><br><br>";
// var_dump($sortedPileOfTools);

		// empty menu entries to build them up manually
		unset( $sortedPileOfTools['general'] );

		// anonymous users
		if ( !$user->isRegistered() ) {
			// remove source-code, history and specialpages from their menus
			unset( $sortedPileOfTools['page-primary']['viewsource'] );
			unset( $sortedPileOfTools['page-primary']['history'] );
			// remove third menu and "more" completely
			$sortedPileOfTools['page-tertiary']	= [];
			$sortedPileOfTools['more']			= [];
			// add links to user registration and login
			$sortedPileOfTools['general']['request-account'] = [
				'text' => 'Benutzerkonto anlegen',
				'href' => '/Spezial:Benutzerkonto_beantragen'
			];
			$sortedPileOfTools['general']['login'] = [
				'text' => 'Anmelden',
				'href' => '/Spezial:Anmelden'
			];
		}

		// logged in users
		if ( $user->isRegistered() ) {
			$sortedPileOfTools['general']['pages-erwin-help'] = [
				'text' => 'Häufige Fragen',
				'href' => '/ErWiN:Hilfe'
			];
			$sortedPileOfTools['general']['pages-help'] = [
				'text' => 'Mediawiki-Hilfe',
				'href' => 'https://www.mediawiki.org/wiki/Help:Contents/de'
			];
			$sortedPileOfTools['general']['pages-sandbox'] = [
				'text' => 'Sandbox',
				'href' => '/ErWiN:Sandbox'
			];
			$sortedPileOfTools['general']['pages-all'] = [
				'text' => 'Alle Seiten',
				'href' => '/Special:AllPages'
			];
			$sortedPileOfTools['general']['specialpages'] = [
				'id'   => 't-specialpages',
				'href' => '/Spezial:Spezialseiten'
			];
		}

		// add some links to special functions for quality guards
		if ( true === in_array( 'quality-guard', $user->getGroups() ) ) {
			$sortedPileOfTools['general']['quality'] = [
				'text' => 'Qualitätssicherung',
				'href' => '/Spezial:Bestätigte_Versionen'
			];
		}

		// add some links to special functions for file managers
		if ( true === in_array( 'file-manager', $user->getGroups() ) ) {
			$sortedPileOfTools['general']['upload'] = [
				'id'   => 't-upload',
				'href' => '/Spezial:Hochladen'
			];
			$sortedPileOfTools['general']['files'] = [
				'text' => 'Dateiliste',
				'href' => '/Spezial:Dateiliste'
			];
			$sortedPileOfTools['general']['files-new'] = [
				'text' => 'Neue Dateien',
				'href' => '/Spezial:Neue_Dateien'
			];
			$sortedPileOfTools['general']['files-statistic'] = [
				'text' => 'Medienstatistik',
				'href' => '/Spezial:Medienstatistiken'
			];
		}

		// add some links to special functions for bureaucrats
		if ( true === in_array( 'bureaucrat', $user->getGroups() ) ) {
			$sortedPileOfTools['general']['user-confirm-request'] = [
				'text' => 'Benutzer freischalten',
				'href' => '/Spezial:Benutzerkonto_best%C3%A4tigen'
			];
			$sortedPileOfTools['general']['user-manage-rights'] = [
				'text' => 'Benutzer verwalten',
				'href' => '/Spezial:Benutzerrechte'
			];
			$sortedPileOfTools['general']['user-list'] = [
				'text' => 'Liste aller Benutzer',
				'href' => '/Spezial:Benutzer'
			];
			$sortedPileOfTools['general']['sendmail'] = [
				'text' => 'E-Mail an Benutzer',
				'href' => '/Spezial:E-Mail_senden'
			];
		}

		return $sortedPileOfTools;
	}

	/**
	 * Generate a Portlet
	 *
	 * @param      string                  $name
	 * @param      array|string            $content
	 * @param      null|string|array|bool  $msg
	 * @param      array                   $setOptions
	 *
	 * @return     string                  html
	 */
	protected function getPortlet( $name, $content, $msg = null, $setOptions = [] ) {

		if ( empty( $content ) ) {
			return '';
		}

		return parent::getPortlet( $name, $content, $msg, $setOptions );
	}

	/**
	 * Generate a Sidebar Chunk
	 *
	 * @param      string	$id
	 * @param      string	$headerMessage
	 * @param      string	$content
	 * @param      array	$classes
	 *
	 * @return     string	html
	 */
	protected function getSidebarChunk( $id, $headerMessage, $content, $classes = [] ) {

		if ( empty( $content ) ) {
			return '';
		}

		return parent::getSidebarChunk( $id, $headerMessage, $content, $classes );
	}

}
