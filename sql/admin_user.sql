-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--
-- admin_user.sql
-- 
-- This MySQL script adds an sysop user to the database
-- with these credentials: Admin // 7GGrkpALiaKN
-- 
-- You only need to run this once after a fresh install.
-- 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `actor` (`actor_id`, `actor_user`, `actor_name`) VALUES
(1,	1,	UNHEX('41646D696E')),
(2,	2,	UNHEX('4D6564696157696B692064656661756C74'));

INSERT INTO `user` (`user_id`, `user_name`, `user_real_name`, `user_password`, `user_newpassword`, `user_newpass_time`, `user_email`, `user_touched`, `user_token`, `user_email_authenticated`, `user_email_token`, `user_email_token_expires`, `user_registration`, `user_editcount`, `user_password_expires`) VALUES
(1,	UNHEX('41646D696E'),	UNHEX(''),	':pbkdf2:sha512:30000:64:Dn2dT9IIJ6FsjlVXjsnLMA==:eIncxmPB1VqtjapiUn7J/wQr2sptCWJGck+CBY+dRFTGi5c2nExB4DouPWzO1H3TlAguPVZRrDRqInerOZ0ISQ==',	'',	NULL,	'none@example.com',	UNHEX('3230323030353131313531363438'),	UNHEX('6465366462353034353139313261393134623565373134616130323136303233'),	NULL,	NULL,	NULL,	UNHEX('3230323030353131313531363436'),	0,	NULL),
(2,	UNHEX('4D6564696157696B692064656661756C74'),	UNHEX(''),	'',	'',	NULL,	'',	UNHEX('3230323030353131313531363436'),	UNHEX('2A2A2A20494E56414C4944202A2A2A0000000000000000000000000000000000'),	NULL,	NULL,	NULL,	UNHEX('3230323030353131313531363436'),	0,	NULL);

INSERT INTO `user_groups` (`ug_user`, `ug_group`, `ug_expiry`) VALUES
(1,	UNHEX('62757265617563726174'),	NULL),
(1,	UNHEX('696E746572666163652D61646D696E'),	NULL),
(1,	UNHEX('7379736F70'),	NULL);

