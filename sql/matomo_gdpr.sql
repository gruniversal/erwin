-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--
-- matomo_gdpr.sql
-- 
-- These SQL statements can be applied on your matomo database (not ErWiN! database)
-- to set some options to assure a cookieless and gdpr compliant tracking.
-- 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE IF NOT EXISTS `matomo_option` (
  `option_name` varchar(191) NOT NULL,
  `option_value` longtext NOT NULL,
  `autoload` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- force cookie-less tracking
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('PrivacyManager.forceCookielessTracking', '1');

-- completely mask ip address
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('PrivacyManager.ipAddressMaskLength', '4');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('PrivacyManager.ipAnonymizerEnabled', '1');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('PrivacyManager.useAnonymizedIpForVisitEnrichment', '1');

-- anonymize referrer url (keep only domain)
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('PrivacyManager.anonymizeReferrer', 'exclude_path');

-- respect Do-Not-Track if sent (opt-out)
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('PrivacyManager.doNotTrackEnabled', '1');

-- delete log data after 550 days (~18 months)
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_logs_enable', '1');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_logs_older_than', '550');

-- delete report data after 18 months
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_reports_enable', '1');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_reports_older_than', '18');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_reports_keep_basic_metrics', '0');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_reports_keep_day_reports', '0');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_reports_keep_month_reports', '0');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_reports_keep_range_reports', '0');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_reports_keep_segment_reports', '0');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_reports_keep_week_reports', '0');
REPLACE INTO `matomo_option` (option_name, option_value) VALUES ('delete_reports_keep_year_reports', '0');



