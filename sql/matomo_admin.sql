-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--
-- matomo_admin.sql
-- 
-- This MySQL script adds an admin user to the database
-- with these credentials: root // 7GGrkpALiaKN
-- 
-- You only need to run this once after a fresh install.
-- 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS `matomo_user` (
  `login` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `twofactor_secret` varchar(40) NOT NULL DEFAULT '',
  `superuser_access` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `date_registered` timestamp NULL DEFAULT NULL,
  `ts_password_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `matomo_user` (`login`, `password`, `email`, `twofactor_secret`, `superuser_access`, `date_registered`, `ts_password_modified`) VALUES
('anonymous',	'',	'anonymous@example.org',	'',	0,	'2021-01-23 18:00:40',	'2021-01-23 18:00:40'),
('root',	'$2y$10$5g0XtHQRveLw5zEugAC92OcGJVfAcqBeD//VQKhSWGYDnWDwyKStK',	'root@example.org',	'',	1,	'2021-01-23 18:01:32',	'2021-01-28 20:07:13');

