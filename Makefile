##
## GNU makefile
##
## This Makefile offers a build process for ErWiN for local development.
##
## First use you should set the desired versions and update vendor packages:
## $ make update-vendors
##
## When everything is in place you can start building your local ErWiN:
## $ make build-erwin
##
## Update your local build after code changes in src: (or use autobuild.sh)
## $ make update-build
##

### Set desired vendor versions

# Mediawiki (see patch 0001)
mediawiki-main-version = 1.35
mediawiki-full-version = 1.35.12

# Extension MsUpload
ext-msu-version = REL1_35

# Extension ConfirmAccount
ext-cfa-version = REL1_35

# Extension ApprovedRevs (see patch 0002)
ext-apr-version = REL1_35

# Extension OpenGraphMeta
ext-ogm-version = REL1_35

# Extension Description2
ext-ds2-version = REL1_35

# Extension DynamicPageList3
ext-dpl-version = REL1_35

# Extension Matomo (use github "master" branch)
ext-mat-version = master

# Skin Timeless
skn-tim-version = REL1_35

# Matomo (use "latest" or semver: "4.1.1")
matomo-version = latest

# Codesniffer
codesniffer-version = 35.0.0

### Temporary Files

timestamp := $(shell date +%Y%m%d_%H%M%S)

builddir := ./build
tempdir := $(builddir)/tempdir_$(timestamp)
tempfile := $(builddir)/tempfile_$(timestamp)

clear-temp:
	@echo "\e[0;33m>>> cleaning temp directory\e[0m"
	rm -rf $(builddir)/temp*

### VENDOR REQUIREMENTS ###

# update all MediaWiki vendor files (except Matomo core)
update-vendors:	update-mediawiki update-ext-msupload update-ext-confirmaccount update-ext-approvedrevs update-ext-opengraphmeta update-ext-description2 update-ext-dynamicpagelist update-ext-matomo update-codesniffer update-skin-timeless clear-temp

## Mediawiki

# this updates vendor/mediawiki to the given version
update-mediawiki:
	@echo "\e[0;33m>>> update vendor/mediawiki to version $(mediawiki-full-version)\e[0m"
	wget -O $(tempfile) https://releases.wikimedia.org/mediawiki/$(mediawiki-main-version)/mediawiki-$(mediawiki-full-version).tar.gz
	tar -xzf $(tempfile) -C vendor/
	rm -rf vendor/mediawiki
	mv -f vendor/mediawiki-$(mediawiki-full-version) vendor/mediawiki
	rm $(tempfile)

## Extensions

# this updates vendor/extensions/MsUpload to the given version
update-ext-msupload:
	@echo "\e[0;33m>>> update vendor/extensions/MsUpload to version $(ext-msu-version)\e[0m"
	wget -O $(tempfile) https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/extensions/MsUpload/+archive/refs/heads/$(ext-msu-version).tar.gz
	mkdir $(tempdir)
	tar -xzf $(tempfile) -C $(tempdir)
	rm -rf vendor/extensions/MsUpload
	mkdir -p vendor/extensions
	mv -f $(tempdir) vendor/extensions/MsUpload
	rm $(tempfile)

# this updates vendor/extensions/ConfirmAccount to the given version
update-ext-confirmaccount:
	@echo "\e[0;33m>>> update vendor/extensions/ConfirmAccount to version $(ext-cfa-version)\e[0m"
	wget -O $(tempfile) https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/extensions/ConfirmAccount/+archive/refs/heads/$(ext-cfa-version).tar.gz
	mkdir $(tempdir)
	tar -xzf $(tempfile) -C $(tempdir)
	rm -rf vendor/extensions/ConfirmAccount
	mkdir -p vendor/extensions
	mv -f $(tempdir) vendor/extensions/ConfirmAccount
	rm $(tempfile)

# this updates vendor/extensions/ApprovedRevs to the given version
update-ext-approvedrevs:
	@echo "\e[0;33m>>> update vendor/extensions/ApprovedRevs to version $(ext-apr-version)\e[0m"
	wget -O $(tempfile) https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/extensions/ApprovedRevs/+archive/refs/heads/$(ext-apr-version).tar.gz
	mkdir $(tempdir)
	tar -xzf $(tempfile) -C $(tempdir)
	rm -rf vendor/extensions/ApprovedRevs
	mkdir -p vendor/extensions
	mv -f $(tempdir) vendor/extensions/ApprovedRevs
	rm $(tempfile)

# this updates vendor/extensions/OpenGraphMeta to the given version
update-ext-opengraphmeta:
	@echo "\e[0;33m>>> update vendor/extensions/OpenGraphMeta to version $(ext-ogm-version)\e[0m"
	wget -O $(tempfile) https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/extensions/OpenGraphMeta/+archive/refs/heads/$(ext-ogm-version).tar.gz
	mkdir $(tempdir)
	tar -xzf $(tempfile) -C $(tempdir)
	rm -rf vendor/extensions/OpenGraphMeta
	mkdir -p vendor/extensions
	mv -f $(tempdir) vendor/extensions/OpenGraphMeta
	rm $(tempfile)

# this updates vendor/extensions/Description2 to the given version
update-ext-description2:
	@echo "\e[0;33m>>> update vendor/extensions/Description2 to version $(ext-ogm-version)\e[0m"
	wget -O $(tempfile) https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/extensions/Description2/+archive/refs/heads/$(ext-ogm-version).tar.gz
	mkdir $(tempdir)
	tar -xzf $(tempfile) -C $(tempdir)
	rm -rf vendor/extensions/Description2
	mkdir -p vendor/extensions
	mv -f $(tempdir) vendor/extensions/Description2
	rm $(tempfile)

# this updates vendor/extensions/DynamicPageList to the given version (from github, not gerrit)
update-ext-dynamicpagelist:
	@echo "\e[0;33m>>> update vendor/extensions/DynamicPageList to version $(ext-dpl-version)\e[0m"
	wget -O $(tempfile) https://github.com/Universal-Omega/DynamicPageList3/archive/refs/heads/$(ext-dpl-version).zip
	mkdir $(tempdir)
	unzip $(tempfile) -d $(tempdir)
	rm -rf vendor/extensions/DynamicPageList
	mkdir -p vendor/extensions
	mv -f $(tempdir)/DynamicPageList3-$(ext-dpl-version) vendor/extensions/DynamicPageList
	rm -rf $(tempdir)
	rm $(tempfile)

# this updates vendor/extensions/Matomo to the given version (from github, not gerrit)
update-ext-matomo:
	@echo "\e[0;33m>>> update vendor/extensions/Matomo to version $(ext-mat-version)\e[0m"
	wget -O $(tempfile) https://github.com/DaSchTour/matomo-mediawiki-extension/archive/$(ext-mat-version).tar.gz
	mkdir $(tempdir)
	tar -xzf $(tempfile) -C $(tempdir)
	rm -rf vendor/extensions/Matomo
	mkdir -p vendor/extensions
	mv -f $(tempdir)/matomo-mediawiki-extension-$(ext-mat-version) vendor/extensions/Matomo
	rm -rf $(tempdir)
	rm $(tempfile)

## Skins

# this updates vendor/skins/Timeless to the given version
update-skin-timeless:
	@echo "\e[0;33m>>> update vendor/skins/Timeless to version $(skn-tim-version)\e[0m"
	wget -O $(tempfile) https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/skins/Timeless/+archive/refs/heads/$(skn-tim-version).tar.gz
	mkdir $(tempdir)
	tar -xzf $(tempfile) -C $(tempdir)
	rm -rf vendor/skins/Timeless
	mkdir -p vendor/skins
	mv -f $(tempdir) vendor/skins/Timeless
	rm $(tempfile)

## Matomo

# this updates vendor/matomo to the given version
update-matomo:
	@echo "\e[0;33m>>> update vendor/matomo to version $(matomo-version)\e[0m"
	wget -O $(tempfile) https://builds.matomo.org/matomo-$(matomo-version).tar.gz
	mkdir $(tempdir)
	rm -rf vendor/matomo
	tar -xzf $(tempfile) -C vendor/
	rm $(tempfile)
	rm vendor/"How to install Matomo.html"

symlink-matomo:
	@echo "\e[0;33m>>> symlink matomo in httpdocs to vendor/matomo\e[0m"
	rm -rf httpdocs/matomo
	ln -s ../vendor/matomo httpdocs/
	@echo "\e[0;33m>>> symlink matomo config in httpdocs to var/matomo\e[0m"
	rm -rf httpdocs/matomo/config/config.ini.php
	ln -s ../../../var/matomo/config.ini.php httpdocs/matomo/config/

## Codesniffer

# this updates vendor/tools/codesniffer to the given version (uses composer)
update-codesniffer:
	@echo "\e[0;33m>>> update vendor/tools/codesniffer to version $(codesniffer-version)\e[0m"
	wget -O $(tempfile) https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/tools/codesniffer/+archive/refs/tags/v$(codesniffer-version).tar.gz
	mkdir $(tempdir)
	tar -xzf $(tempfile) -C $(tempdir)
	rm -rf vendor/tools/codesniffer
	mkdir -p vendor/tools
	mv -f $(tempdir) vendor/tools/codesniffer
	rm $(tempfile)
	composer install --no-dev --working-dir vendor/tools/codesniffer

# check code against (most of) MediaWiki Standard
check-code: check-code-skins check-code-extensions

check-code-skins:
	phpcs --standard=vendor/tools/codesniffer/MediaWiki --bootstrap=vendor/tools/codesniffer/vendor/autoload.php --exclude=Generic.ControlStructures.DisallowYodaConditions,MediaWiki.WhiteSpace.DisallowEmptyLineFunctions,Generic.Formatting.SpaceAfterCast,MediaWiki.Usage.ForbiddenFunctions,MediaWiki.Usage.IsNull,MediaWiki.Commenting.FunctionComment,MediaWiki.Commenting.PropertyDocumentation,MediaWiki.Commenting.LicenseComment,MediaWiki.WhiteSpace.SpaceyParenthesis,MediaWiki.WhiteSpace.SpaceBeforeSingleLineComment,Generic.Formatting.MultipleStatementAlignment -s src/skins/ || true

check-code-extensions:
	phpcs --standard=vendor/tools/codesniffer/MediaWiki --bootstrap=vendor/tools/codesniffer/vendor/autoload.php --exclude=Generic.ControlStructures.DisallowYodaConditions,MediaWiki.WhiteSpace.DisallowEmptyLineFunctions,Generic.Formatting.SpaceAfterCast,MediaWiki.Usage.ForbiddenFunctions,MediaWiki.Usage.IsNull,MediaWiki.Commenting.FunctionComment,MediaWiki.Commenting.PropertyDocumentation,MediaWiki.Commenting.LicenseComment,MediaWiki.WhiteSpace.SpaceyParenthesis,MediaWiki.WhiteSpace.SpaceBeforeSingleLineComment,Generic.Formatting.MultipleStatementAlignment -s src/extensions/ || true

### BUILDING ###

# default: build a shrinked ErWiN instance (for upload purposes)
build-erwin: build-httpdocs shrink-build patch-build symlink-images symlink-matomo update-build

# build a shrinked and minified ErWiN instance (experimental, very little effect since mediawiki's resource loading mechanism is pretty cool)
build-erwin-minify: build-httpdocs shrink-build patch-build symlink-images symlink-matomo update-build minify-js minify-css

# build a non-shrinked ErWiN instance
build-full-erwin: build-httpdocs patch-build symlink-images symlink-matomo update-build

# build a shrinked ErWiN instance with installer and maintenance tools
build-maintain-erwin: build-httpdocs shrink-build-maintain patch-build symlink-images symlink-matomo update-build

# main build process
build-httpdocs:
	@echo "\e[0;33m***\e[0m"
	@echo "\e[0;33m*** !! Warning: This will replace the complete httpdocs folder with a fresh install !!\e[0m"
	@echo "\e[0;33m*** Your current ErWiN installation will be saved to: httpdocs_backup_$(timestamp)\e[0m"
	@echo "\e[0;33m***\e[0m"
	@echo "\e[0;33m*** (any key to continue or Ctrl-C to abort) \e[0m"
	@echo "\e[0;33m***\e[0m"
	@read x

	@echo "\e[0;33m>>> backup old httpdocs to httpdocs_backup_$(timestamp)\e[0m"
	mv httpdocs httpdocs_backup_$(timestamp)

	@echo "\e[0;33m>>> create a fresh httpdocs folder\e[0m"
	mkdir httpdocs

	@echo "\e[0;33m>>> preserve README.md\e[0m"
	-cp httpdocs_backup_$(timestamp)/README.md httpdocs/README.md

	@echo "\e[0;33m>>> copy mediawiki to httpdocs\e[0m"
	cp -r vendor/mediawiki/* httpdocs/

	@echo "\e[0;33m>>> replace extensions in httpdocs\e[0m"
	rm -rf httpdocs/extensions
	mkdir httpdocs/extensions
	cp -r vendor/mediawiki/extensions/WikiEditor httpdocs/extensions/WikiEditor
	cp -r vendor/mediawiki/extensions/ConfirmEdit httpdocs/extensions/ConfirmEdit
	cp -r vendor/mediawiki/extensions/PageImages httpdocs/extensions/PageImages
	cp -r vendor/mediawiki/extensions/ParserFunctions httpdocs/extensions/ParserFunctions
	cp -r vendor/mediawiki/extensions/Cite httpdocs/extensions/Cite
	cp -r vendor/extensions/MsUpload httpdocs/extensions/MsUpload
	cp -r vendor/extensions/ConfirmAccount httpdocs/extensions/ConfirmAccount
	cp -r vendor/extensions/ApprovedRevs httpdocs/extensions/ApprovedRevs
	cp -r vendor/extensions/OpenGraphMeta httpdocs/extensions/OpenGraphMeta
	cp -r vendor/extensions/Description2 httpdocs/extensions/Description2
	cp -r vendor/extensions/DynamicPageList httpdocs/extensions/DynamicPageList
	#disabled, now use own version in src/extensions/Matomo
	#cp -r vendor/extensions/Matomo httpdocs/extensions/Matomo

	@echo "\e[0;33m>>> replace skins in httpdocs\e[0m"
	rm -rf httpdocs/skins
	cp -r vendor/skins httpdocs/

symlink-images:
	@echo "\e[0;33m>>> replace imagefolder in httpdocs with symlink\e[0m"
	rm -rf httpdocs/images
	ln -s ../var/images httpdocs/

### SHRINKING ###

# default:
shrink-build: shrink-all

# remove all (better: a lot of) unnecessary files from build
shrink-all: shrink-installer shrink-maintenance shrink-build-files shrink-tests shrink-docs shrink-languages shrink-parsoid

# remove unnecessary files from build but maintain installer and maintenance
shrink-build-maintain: shrink-build-files shrink-tests shrink-docs shrink-languages shrink-parsoid

shrink-installer:
	@echo "\e[0;33m>>> delete mediawiki installer from httpdocs (using manual installation)\e[0m"
	rm -rf httpdocs/mw-config

shrink-maintenance:
	@echo "\e[0;33m>>> delete maintenance from httpdocs (must be run in cli mode, not available in final environment)\e[0m"
	rm -rf httpdocs/maintenance

shrink-build-files:
	@echo "\e[0;33m>>> delete build related files from httpdocs\e[0m"
	rm -rf httpdocs/.pipeline
	find httpdocs -name "composer.json" -exec rm {} +
	find httpdocs -name "composer.lock" -exec rm {} +
	find httpdocs -name "Gruntfile.js" -exec rm {} +
	find httpdocs -name "package-lock.json" -exec rm {} +
	find httpdocs -name "Makefile" -exec rm {} +
	find httpdocs -name "Rakefile" -exec rm {} +
	find httpdocs -name "jsduck.json" -exec rm {} +
	find httpdocs -name "*.json-sample" -exec rm {} +
	find httpdocs -name ".phpcs.xml" -exec rm {} +
	find httpdocs -name ".eslintrc.json" -exec rm {} +
	find httpdocs -name "package.json" -exec rm {} +
	find httpdocs -name ".gitignore" -exec rm {} +
	find httpdocs -name ".gitattributes" -exec rm {} +
	find httpdocs -path "*/.github" -exec rm -r {} +

shrink-tests:
	@echo "\e[0;33m>>> delete test related files from httpdocs\e[0m"
	rm -rf httpdocs/tests
	find httpdocs -path "*/tests" -exec rm -r {} +
	find httpdocs -name "phpunit.xml" -exec rm {} +
	find httpdocs -path "*/.phan" -exec rm -r {} +

shrink-docs:
	@echo "\e[0;33m>>> delete documentation related files from httpdocs\e[0m"
	rm -rf httpdocs/docs
	find httpdocs -name "CODE_OF_CONDUCT*" -exec rm {} +
	find httpdocs -name "CHANGELOG*" -exec rm {} +
	find httpdocs -name "CONTRIBUTING*" -exec rm {} +
	find httpdocs -name "HISTORY*" -exec rm {} +
	find httpdocs -name "History.md" -exec rm {} +
	find httpdocs -name "LICENSE*" -exec rm {} +
	find httpdocs -name "COPYING*" -exec rm {} +
	find httpdocs -name "UPGRAD*" -exec rm {} +
	find httpdocs -name "AUTHORS*" -exec rm {} +

shrink-languages:
	@echo "\e[0;33m>>> delete unnecessary language files from httpdocs (leave only de and en)\e[0m"
	find . -regextype posix-extended -regex '\.\/httpdocs.*i18n\/(.*)\.json$$' -print | grep --perl-regex --invert-match '^.*i18n\/(.*\/)?(de|en)\.json$$' | xargs rm -f
	find . -regextype posix-extended -regex '\.\/httpdocs.*languages\/messages/Message(.*)\.php$$' -print | grep --perl-regex --invert-match '^.*messages\/Messages(De|En)\.php$$' | xargs rm -f
	find . -regextype posix-extended -regex '\.\/httpdocs.*languages\/classes/Language(.*)\.php$$' -print | grep --perl-regex --invert-match '^.*classes\/Language(De|En)\.php$$' | xargs rm -f
	find . -regextype posix-extended -regex '\.\/httpdocs\/resources\/lib\/jquery.i18n\/src\/languages\/(.*)\.js$$' -print | grep --perl-regex --invert-match '^.*languages\/(de|en)(.*)\.js$$' | xargs rm -f
	find . -regextype posix-extended -regex '\.\/httpdocs\/resources\/lib\/jquery.ui\/i18n\/jquery.ui.datepicker-(.*)\.js$$' -print | grep --perl-regex --invert-match '^.*jquery.ui.datepicker-(de|en)(.*)\.js$$' | xargs rm -f
	find . -regextype posix-extended -regex '\.\/httpdocs\/resources\/lib\/moment\/locale\/(.*)\.js$$' -print | grep --perl-regex --invert-match '^.*locale\/(de|en)(.*)\.js$$' | xargs rm -f
	find . -regextype posix-extended -regex '\.\/httpdocs\/resources\/src\/mediawiki.language\/languages\/(.*)\.js$$' -print | grep --perl-regex --invert-match '^.*languages\/(de|en)(.*)\.js$$' | xargs rm -f

shrink-parsoid:
	@echo "\e[0;33m>>> delete parsoid from httpdocs (as we don't use the VisualEditor extension)\e[0m"
	rm -rf httpdocs/vendor/wikimedia/parsoid

### MINIFYING ###

minify-js:
	@echo "\e[0;33m>>> minify js files\e[0m"
	find httpdocs -name "*.js" -exec "./minify.sh" {} \;

minify-css:
	@echo "\e[0;33m>>> minify css files\e[0m"
	find httpdocs -name "*.css" -exec "./minify.sh" {} \;

### PATCHING ###

# apply MediaWiki core and extension patches
patch-build:
	@echo "\e[0;33m>>> apply MediaWiki core patches\e[0m"
	patch -p1 < src/patches/0001-fixed-Special-FileList-and-Special-NewFiles.patch

	@echo "\e[0;33m>>> apply MediaWiki extension patches\e[0m"
	patch -p1 < src/patches/0002_ApprovedRevs_configure_BlankPage.patch

### OVERWRITING ###

# updates build during development
update-build:
	@echo "\e[0;33m>>> update source files in httpdocs (overlay vendor files)\e[0m"
	cp -rf src/LocalSettings.php httpdocs/LocalSettings.php
	cp -rf src/erwin httpdocs/
	cp -rf src/extensions/ httpdocs/
	cp -rf src/languages/ httpdocs/
	cp -rf src/skins/ httpdocs/
	cp -rf src/htaccess/.htaccess httpdocs/

### BACKUPS ###

# clean up backups made with build-erwin
delete-backups:
	@echo "\e[0;33m***\e[0m"
	@echo "\e[0;33m*** !! Warning: This will delete all httpdocs folder backups !!\e[0m"
	@echo "\e[0;33m***\e[0m"
	@echo "\e[0;33m*** These backup folders were found:\e[0m"
	@ls -dl httpdocs_backup_*
	@echo "\e[0;33m***\e[0m"
	@echo "\e[0;33m*** (any key to continue or Ctrl-C to abort) \e[0m"
	@echo "\e[0;33m***\e[0m"
	@read x
	@echo "\e[0;33m>>> delete all httpdocs folder backups\e[0m"
	rm -rf httpdocs_backup_*

# retrieve database from LIVE via ftp-mysql-dump
# @see: https://gitlab.com/gruniversal/ftp-mysql-dump
# @see: https://blog.gruniversal.de/2020/06/15/ftp-mysql-dump-eine-datenbank-per-ftp-abholen/
retrieve-database:
	ftp-mysql-dump
	mv dump_*.sql.zip .sqldump/

### DEPLOYMENT ###

##
## The deployment process relies on the git-ftp tool:
## https://github.com/git-ftp/git-ftp
##
## @see: https://gitlab.com/gruniversal/erwin#deployment
##

deploy-dry-run:
	@echo "\e[0;33m>>> deploy: DRY RUN\e[0m"
	git-ftp push --dry-run

deploy-live:
	@echo "\e[0;33m>>> deploy: LIVE\e[0m"
	git-ftp push | tee "./build/deploy_$(timestamp).live"
