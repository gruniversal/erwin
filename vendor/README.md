# vendor

This directory contains vendor packages that are required for ErWiN.

It is not intended to alter these files manually.

Use the [Makefile](../Makefile) to update the vendor packages:

`$ make update-vendors`