# var/images

If uploads are enabled in the wiki, files will be put in subdirectories under here.

This folder is symlinked to the httpdocs folder to preserve the uploaded files when ErWiN is built.

See the [Makefile](../../Makefile) for more information.

